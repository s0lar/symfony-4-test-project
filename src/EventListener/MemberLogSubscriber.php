<?php

namespace App\EventListener;

use App\Entity\Member;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class MemberLogSubscriber implements EventSubscriber
{
    /**
     * Подписываемся на событие
     *
     * @return void
     */
    public function getSubscribedEvents()
    {
        return array(
            'preUpdate',
        );
    }

    /**
     * Логируем измениня в ЧП
     *
     * @param PreUpdateEventArgs $eventArgs
     * @return void
     */
    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        if ($eventArgs->getEntity() instanceof Member) {

            // Логируем смену статуса
            if ($eventArgs->hasChangedField('status')) {
                $entity = $eventArgs->getEntity();

                $entity->addLog([
                    'date' => new \DateTime(),
                    'message' => sprintf(
                        'Смена статуса "%s" -> "%s"',
                        $entity->getTextStatus($eventArgs->getOldValue('status')),
                        $entity->getTextStatus($eventArgs->getNewValue('status'))
                    ),
                ]);
            }

            // Логируем смену ПО
            if ($eventArgs->hasChangedField('po')) {
                $entity = $eventArgs->getEntity();

                $entity->addLog([
                    'date' => new \DateTime(),
                    'message' => sprintf(
                        'Смена ПО "%s" -> "%s"',
                        ($eventArgs->getOldValue('po') !== null ? $eventArgs->getOldValue('po')->getTitle() : '-'),
                        ($eventArgs->getNewValue('po') !== null ? $eventArgs->getNewValue('po')->getTitle() : '-')
                    ),
                ]);
            }
        }
    }
}
