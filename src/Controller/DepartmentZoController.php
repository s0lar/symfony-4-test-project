<?php

namespace App\Controller;

use App\Entity\DepartmentZo;
use App\Form\DepartmentZoType;
use App\Repository\DepartmentZoRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/department/zo")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DepartmentZoController extends Controller
{
    /**
     * @Route("/", name="department_zo_index", methods="GET")
     */
    public function index(DepartmentZoRepository $departmentZoRepository): Response
    {
        return $this->render('department_zo/index.html.twig', ['department_zos' => $departmentZoRepository->findAll()]);
    }

    /**
     * @Route("/new", name="department_zo_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $departmentZo = new DepartmentZo();
        $form = $this->createForm(DepartmentZoType::class, $departmentZo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($departmentZo);
            $em->flush();

            return $this->redirectToRoute('department_zo_index');
        }

        return $this->render('department_zo/new.html.twig', [
            'department_zo' => $departmentZo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_zo_show", methods="GET")
     */
    public function show(DepartmentZo $departmentZo): Response
    {
        return $this->render('department_zo/show.html.twig', ['department_zo' => $departmentZo]);
    }

    /**
     * @Route("/{id}/edit", name="department_zo_edit", methods="GET|POST")
     */
    public function edit(Request $request, DepartmentZo $departmentZo): Response
    {
        $form = $this->createForm(DepartmentZoType::class, $departmentZo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('department_zo_edit', ['id' => $departmentZo->getId()]);
        }

        return $this->render('department_zo/edit.html.twig', [
            'department_zo' => $departmentZo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_zo_delete", methods="DELETE")
     */
    public function delete(Request $request, DepartmentZo $departmentZo): Response
    {
        if ($this->isCsrfTokenValid('delete' . $departmentZo->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($departmentZo);
            $em->flush();
        }

        return $this->redirectToRoute('department_zo_index');
    }
}
