<?php

namespace App\Controller;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\DepartmentRo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Контроллер списков отделений
 *
 * @Route("/departments")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DepartmentController extends Controller
{
    /**
     * Хлебные крошки
     *
     * @param DepartmentRo $ro
     * @param DepartmentMo $mo
     * @param DepartmentPo $po
     * @return array
     */
    private function getBreadCrumbs(DepartmentRo $ro = null, DepartmentMo $mo = null, DepartmentPo $po = null): array
    {
        //  Первый уровень
        $bc[] = array(
            'name' => 'Отделения',
            'link' => $this->generateUrl('department_index'),
        );

        //  РО
        if ($ro) {
            $bc[] = array(
                'name' => $ro->getTitle(),
                'link' => $this->generateUrl('department_ro', ['ro' => $ro->getId()]),
            );
        }

        //  Мо
        if ($mo) {
            $bc[] = array(
                'name' => $mo->getTitle(),
                'link' => $this->generateUrl('department_mo', ['ro' => $ro->getId(), 'mo' => $mo->getId()]),
            );
        }

        //  По
        if ($po) {
            $bc[] = array(
                'name' => $po->getTitle(),
                'link' => $this->generateUrl('department_po', ['ro' => $ro->getId(), 'mo' => $mo->getId(), 'po' => $po->getId()]),
            );
        }

        return $bc;
    }

    /**
     * Показать Муниципальные образования
     *
     * @Route("/", name="department_index")
     */
    public function index()
    {
        //  Выбрать все РО
        $list = $this->getDoctrine()->getRepository(DepartmentRo::class)->findAll();

        return $this->render('department/index.html.twig', array(
            'list' => $list,
            'path' => $this->generateUrl('department_index'),
            'bread_crumbs' => $this->getBreadCrumbs(),
        ));
    }

    /**
     * Показать Муниципальные образования
     *
     * @Route("/{ro}/", name="department_ro")
     * @ParamConverter("ro", class="App:DepartmentRo", options={"mapping": {"ro": "id"}})
     */
    public function roShow(DepartmentRo $ro)
    {
        //  Выбрать все МО
        $list = $this->getDoctrine()->getRepository(DepartmentMo::class)->findByRoId($ro->getId());

        //  Посчитать все ПО
        $poCount = $this->getDoctrine()->getRepository(DepartmentPo::class)->poCountByMo();

        return $this->render('department/index.html.twig', array(
            'list' => $list,
            'path' => $this->generateUrl('department_ro', ['ro' => $ro->getId()]),
            'path_edit' => 'department_mo_edit',
            'path_new' => 'department_mo_new',
            'bread_crumbs' => $this->getBreadCrumbs($ro),
            'po_count' => $poCount,
        ));
    }

    /**
     * Показать Муниципальные образования
     *
     * @Route("/{ro}/{mo}/", name="department_mo")
     * @ParamConverter("ro", class="App:DepartmentRo", options={"mapping": {"ro": "id"}})
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     */
    public function moShow(DepartmentRo $ro, DepartmentMo $mo)
    {
        //  Выбрать все первичные отделения
        $poList = $this->getDoctrine()->getRepository(DepartmentPo::class)->findByMoId($mo->getId(), ['title' => 'ASC']);

        return $this->render('department/index.html.twig', array(
            'list' => $poList,
            'path_edit' => 'department_po_edit',
            'path_new' => 'department_po_new',
            'bread_crumbs' => $this->getBreadCrumbs($ro, $mo),
        ));
    }

    /**
     * Показать Муниципальные образования
     *
     * @Route("/{ro}/{mo}/{po}/", name="department_po")
     * @ParamConverter("ro", class="App:DepartmentRo", options={"mapping": {"ro": "id"}})
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     * @ParamConverter("po", class="App:DepartmentPo", options={"mapping": {"po": "id"}})
     */
    public function poShow(DepartmentRo $ro, DepartmentMo $mo, DepartmentPo $po)
    {
        return $this->render('department/index.html.twig', array(
            'bread_crumbs' => $this->getBreadCrumbs($ro, $mo, $po),
        ));
    }
}
