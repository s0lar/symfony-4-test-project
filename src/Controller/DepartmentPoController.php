<?php

namespace App\Controller;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\DepartmentZo;
use App\Form\DepartmentPoType;
use App\Repository\DepartmentPoRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/department/po")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DepartmentPoController extends Controller
{
    /**
     * @Route("/", name="department_po_index", methods="GET")
     */
    public function index(DepartmentPoRepository $departmentPoRepository): Response
    {
        return $this->render('department_po/index.html.twig', ['department_pos' => $departmentPoRepository->findAll()]);
    }

    /**
     * @Route("/new", name="department_po_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $departmentPo = new DepartmentPo();

        $mo = $this->getDoctrine()->getManager()
            ->getRepository(DepartmentMo::class)->loadMO();

        $zo = $this->getDoctrine()->getManager()
            ->getRepository(DepartmentZo::class)->loadZO();

        $form = $this->createForm(DepartmentPoType::class, $departmentPo, ['moId' => $mo, 'zoId' => $zo]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($departmentPo);
            $em->flush();

            return $this->redirectToRoute('department_mo', ['ro' => 23, 'mo' => $departmentPo->getMoId()]);
        }

        return $this->render('department_po/new.html.twig', [
            'department_po' => $departmentPo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_po_show", methods="GET")
     */
    public function show(DepartmentPo $departmentPo): Response
    {
        return $this->render('department_po/show.html.twig', ['department_po' => $departmentPo]);
    }

    /**
     * @Route("/{id}/edit", name="department_po_edit", methods="GET|POST")
     */
    public function edit(Request $request, DepartmentPo $departmentPo): Response
    {
        $mo = $this->getDoctrine()->getManager()
            ->getRepository(DepartmentMo::class)->loadMO();

        $zo = $this->getDoctrine()->getManager()
            ->getRepository(DepartmentZo::class)->loadZO();

        $form = $this->createForm(DepartmentPoType::class, $departmentPo, ['moId' => $mo, 'zoId' => $zo]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('department_po_edit', ['id' => $departmentPo->getId()]);
        }

        return $this->render('department_po/edit.html.twig', [
            'department_po' => $departmentPo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_po_delete", methods="DELETE")
     */
    public function delete(Request $request, DepartmentPo $departmentPo): Response
    {
        if ($this->isCsrfTokenValid('delete' . $departmentPo->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($departmentPo);
            $em->flush();
        }

        return $this->redirectToRoute('department_mo', ['ro' => 23, 'mo' => $departmentPo->getMoId()]);
    }
}
