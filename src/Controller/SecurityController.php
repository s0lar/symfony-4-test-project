<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AuthNewPasswordType;
use App\Form\AuthRegistryType;
use App\Form\AuthResetType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Translation\TranslatorInterface;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/reset", name="reset")
     */
    public function reset(Request $request, \Swift_Mailer $mailer, TranslatorInterface $translator)
    {
        //  Error messages
        $error = '';

        //  Create Form
        $form = $this->createForm(AuthResetType::class, null, [
            'action' => $this->generateUrl('reset'),
            'method' => 'POST',
        ]);

        //  Bind request
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //  Get POST data
            $data = $form->getData();

            //  Find User by email or username
            $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->loadUserByUsername($data['email']);

            //  Error. If empty user
            if (empty($user)) {
                $error = 'security.error.user.notFound';
            }

            //  Error. User disabled
            elseif (!$user->isEnabled()) {
                $error = 'security.error.user.disabled';
            }

            //  Error. User blocked
            elseif (!$user->isAccountNonLocked()) {
                $error = 'security.error.user.locked';
            }

            //  Success. Generate resetUrl
            else {
                //  Generate reset url
                $user->setResetUrl(bin2hex(random_bytes(32)));

                //  Link will work for 10 minutes
                $user->setResetExpireAt(new \DateTime('+10min'));

                //  Save
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                //  Generate reset URL
                $link = $request->getSchemeAndHttpHost() .
                $this->generateUrl('newPassword', ['resetUrl' => $user->getResetUrl()]);

                //  Send email to user
                $message = (new \Swift_Message($translator->trans('email.reset.subject')))
                    ->setFrom($this->container->getParameter('mailFrom'))
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'emails/reset.html.twig',
                            array(
                                'user' => $user,
                                'link' => $link,
                            )
                        ),
                        'text/html'
                    );

                $mailer->send($message);

                return $this->redirectToRoute('resetSuccess');
            }
        }

        return $this->render('security/reset.html.twig', array(
            'AuthResetForm' => $form->createView(),
            'error' => $error,
        ));
    }

    /**
     * @Route("/resetSuccess", name="resetSuccess")
     */
    public function resetSuccess(Request $request)
    {
        return $this->render('security/resetSuccess.html.twig');
    }

    /**
     * @Route("/newPassword/{resetUrl}", name="newPassword")
     */
    public function newPassword(Request $request, $resetUrl, UserPasswordEncoderInterface $encoder)
    {
        //  Error messages
        $error = '';

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneByResetUrl($resetUrl);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        //  Reset link expired
        elseif ($user->getResetExpireAt() < new \DateTime()) {
            $error = 'security.error.user.resetUrlExpired';
        }

        //  Create Form
        $form = $this->createForm(AuthNewPasswordType::class, $user, [
            'action' => $this->generateUrl('newPassword', ['resetUrl' => $user->getResetUrl()]),
            'method' => 'POST',
        ]);

        //  Bind request
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //  Encode new password
            $encoded = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded);
            $user->setResetUrl('');
            $user->setResetExpireAt(new \DateTime('-1min'));

            //  Save
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('newPasswordSuccess');
        }

        return $this->render('security/newPassword.html.twig', array(
            'AuthNewPasswordForm' => $form->createView(),
            'error' => $error,
        ));
    }

    /**
     * @Route("/newPasswordSuccess", name="newPasswordSuccess")
     */
    public function newPasswordSuccess(Request $request)
    {
        return $this->render('security/newPasswordSuccess.html.twig');
    }

    /**
     * @Route("/registry", name="registry")
     */
    public function registry(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, TranslatorInterface $translator)
    {
        //  Error messages
        $error = '';

        //  New entity
        $user = new User();

        //  Create Form
        $form = $this->createForm(AuthRegistryType::class, $user, [
            'action' => $this->generateUrl('registry'),
            'method' => 'POST',
        ]);

        //  Bind request
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $userExist = $this->getDoctrine()
                ->getRepository(User::class)
                ->checkUserExists($user->getUsername(), $user->getEmail());

            //  User exists error
            if ($userExist) {
                $error = 'security.registry.error.userExist';
            }

            //  Registry new user
            else {
                //  Encode new password
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                //  Set activation URL
                $user->setActivationUrl(bin2hex(random_bytes(32)));

                //  Generate reset URL
                $link = $request->getSchemeAndHttpHost() .
                $this->generateUrl('registryConfirm', ['activationUrl' => $user->getActivationUrl()]);

                //  Save
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                //  Send email to user
                $message = (new \Swift_Message($translator->trans('email.registry.subject')))
                    ->setFrom($this->container->getParameter('mailFrom'))
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'emails/registry.html.twig',
                            array(
                                'user' => $user,
                                'link' => $link,
                            )
                        ),
                        'text/html'
                    );
                $mailer->send($message);

                return $this->redirectToRoute('registrySuccess');
            }
        }

        return $this->render('security/registry.html.twig', array(
            'AuthRegistryForm' => $form->createView(),
            'error' => $error,
        ));
    }

    /**
     * @Route("/registrySuccess", name="registrySuccess")
     */
    public function registrySuccess(Request $request)
    {
        return $this->render('security/registrySuccess.html.twig');
    }

    /**
     * @Route("/registryConfirm/{activationUrl}", name="registryConfirm")
     */
    public function registryConfirm(Request $request, $activationUrl)
    {
        //  Error messages
        $error = '';

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneByActivationUrl($activationUrl);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        //  Activate user
        $user->setIsActive(true);

        //  Clear activation url
        $user->setActivationUrl('');

        //  Save
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->render('security/registryConfirm.html.twig');
    }
}
