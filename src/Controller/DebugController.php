<?php

namespace App\Controller;

use App\Entity\DepartmentPo;
use App\Entity\Member;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Фиксим ошибки в данных после переноса
 *
 * @Route("/debug")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DebugController extends Controller
{
    /**
     * @Route("/", name="debug")
     */
    public function index()
    {
        $members = [];
        $poList = $this->getDoctrine()->getManager()
            ->getRepository(DepartmentPo::class)->findBy([]);

        foreach ($poList as $po) {
            $tmp = $this->getDoctrine()->getManager()
                ->getRepository(Member::class)->findWrongMoPo(
                $po->getMoId(),
                $po->getId()
            );

            if (!empty($tmp)) {
                $members = array_merge($members, $tmp);
            }
        }

        return $this->render('debug/index.html.twig', [
            'members' => $members,
        ]);
    }
}
