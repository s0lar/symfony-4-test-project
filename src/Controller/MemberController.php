<?php

namespace App\Controller;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\DepartmentRo;
use App\Entity\Member;
use App\Entity\User;
use App\Form\MemberFilterType;
use App\Form\MemberType;
use App\Form\TicketsFilterType;
use App\Repository\MemberRepository;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/member")
 * @Security("has_role('ROLE_USER')")
 */
class MemberController extends Controller
{

    /**
     * @Route("/", defaults={"page": "1"},  name="member_index")
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="member_index_paginated")
     */
    public function index(int $page, MemberRepository $repo, Request $request, TokenStorageInterface $token)
    {
        //  Постраничная разбивка
        if ($request->query->get('page')) {
            $page = $request->query->get('page');
        }

        //  Массив фильтром
        $data = [];

        //  Ограничения для ролей МИК
        $user = $token->getToken()->getUser();

        //  Загружаем МО и ПО в зависимости от роли
        if (in_array('ROLE_USER', $user->getRoles()) && $user->getDepartmentMo()) {
            $mo = [
                $user->getDepartmentMo(),
            ];
            $po = $this->getDoctrine()->getRepository(DepartmentPo::class)->findByMoId(
                $user->getDepartmentMo()->getId()
            );

            //  Выбираем Членов партии только по задонному МО
            $data['mo'] = $user->getDepartmentMo();
        } else {
            $po = $this->getDoctrine()->getRepository(DepartmentPo::class)->findAll();
            $mo = $this->getDoctrine()->getRepository(DepartmentMo::class)->findAll();
        }

        //  Создать форму
        $filter = $this->createForm(MemberFilterType::class, null, [
            'action' => $this->generateUrl('member_index'),
            'method' => 'GET',
            'mo' => $mo,
            'po' => $po,
            'department_mo' => $this->getDoctrine()->getRepository(DepartmentMo::class)->loadMO(),
        ]);

        //  Фильтруем Членов партии
        $filter->handleRequest($request);
        if ($filter->isSubmitted() && $filter->isValid()) {
            $data = $filter->getData();

            //  Если нужно сформировать выгрузку в XLS
            if ($filter->getClickedButton() && 'xls_report' === $filter->getClickedButton()->getName()) {
                $response = $this->forward('App\Controller\ReportController::report11_xls', array(
                    'filter' => $data,
                ));

                return $response;
            }
        }

        //  Всегда выбираем Членов партии только по задонному МО
        if (in_array('ROLE_USER', $user->getRoles()) && $user->getDepartmentMo()) {
            $data['mo'] = $user->getDepartmentMo();
        }

        //  Выбираем членов партии
        $members = $repo->findMembers($page, $data);

        return $this->render('member/index.html.twig', array(
            'members' => $members,
            'filter' => $filter->createView(),
        ));
    }

    /**
     * @Route("/create/", name="member_create")
     */
    public function create(Request $request, TokenStorageInterface $token, LoggerInterface $logger)
    {
        //  Объект
        $member = new Member();

        //  Создать форму
        $form = $this->createForm(MemberType::class, $member, [
            'action' => $this->generateUrl('member_create'),
            'method' => 'POST',
            'department_mo' => $this->getDoctrine()->getRepository(DepartmentMo::class)->loadMO(),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //  Устанавливаем Региональное отделение. По-умолчанию 23
            $member->setRo($this->getDoctrine()->getRepository(DepartmentRo::class)->find('23'));

            //  Устанавливаем Местное отделение
            $member->setMo($this->getDoctrine()->getRepository(DepartmentMo::class)->find($member->getPo()->getMoId()));

            //  Кто создает пользователя
            $member->setCreatedBy($token->getToken()->getUser()->getId());

            //  Указываем дату создания
            $member->setCreatedAt(new \DateTime());

            //  Указываем статус анкеты
            $member->setAppStatus(0);

            //  Сохраняем данные
            $em = $this->getDoctrine()->getManager();
            $em->persist($member);
            $em->flush();

            //  Логируем
            $logger->info('Добавлен новый Член партии.', array(
                'ФИО' => trim($member->getLastName() . ' ' . $member->getFirstName() . ' ' . $member->getMiddleName()),
                'user' => array(
                    $token->getToken()->getUser()->getId(),
                    $token->getToken()->getUser()->getUsername(),
                    $token->getToken()->getUser()->getName(),
                ),
            ));

            //  Всплывающее сообщение
            $this->addFlash('primary', 'Новый Член Партии создан');

            //  Редирект
            return $this->redirect($this->generateUrl('member_index'));
        }

        return $this->render('member/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Показать выбранного члена партии
     *
     * @Route("/{id}", name="member_read")
     * @Method("GET")
     * @ParamConverter("member", class="App:member")
     * @Security("member.isMoOwner(user)", message="Нет доступа к Членам Партии других МО")
     */
    public function read(Member $member)
    {
        //  Создать форму
        $form = $this->createForm(MemberType::class, $member, [
            'action' => $this->generateUrl('member_update', ['id' => $member->getId()]),
            'method' => 'POST',
            'department_mo' => $this->getDoctrine()->getRepository(DepartmentMo::class)->loadMO(),
        ]);

        return $this->render('member/read.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }

    /**
     * Изменить Члена партии
     *
     * @Route("/{id}", name="member_update")
     * @Method("POST")
     * @ParamConverter("member", class="App:member")
     * @Security("member.isMoOwner(user)", message="Нельзя вносить изменения в Членах Партии других МО")
     */
    public function update(Request $request, Member $member, TokenStorageInterface $token, LoggerInterface $logger)
    {
        //  Создать форму
        $form = $this->createForm(MemberType::class, $member, [
            'action' => $this->generateUrl('member_update', ['id' => $member->getId()]),
            'method' => 'POST',
            'department_mo' => $this->getDoctrine()->getRepository(DepartmentMo::class)->loadMO(),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //  Устанавливаем Региональное отделение. По-умолчанию 23
            $member->setRo($this->getDoctrine()->getRepository(DepartmentRo::class)->find('23'));

            //  Принудительно устанавливаем МО, если поменяли ПО
            if ($member->getMo()->getId() != $member->getPo()->getMoId()) {
                $member->setMo(
                    $this->getDoctrine()->getRepository(DepartmentMo::class)->find($member->getPo()->getMoId())
                );
            }

            //  Указываем дату обновления
            $member->setUpdatedAt(new \DateTime());

            //  Кто создает пользователя
            $member->setUpdatedBy($token->getToken()->getUser()->getId());

            //  Сохраняем данные
            $em = $this->getDoctrine()->getManager();
            $em->persist($member);
            $em->flush();

            //  Логируем
            $logger->info('Изменен Член партии.', array(
                'ФИО' => trim($member->getLastName() . ' ' . $member->getFirstName() . ' ' . $member->getMiddleName()),
                'user' => array(
                    $token->getToken()->getUser()->getId(),
                    $token->getToken()->getUser()->getUsername(),
                    $token->getToken()->getUser()->getName(),
                ),
            ));

            //  Всплывающее сообщение
            $this->addFlash('primary', 'Изменения сохранены');
        }

        return $this->render('member/read.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }

    /**
     * Удалить члена партии
     *
     * @Route("/{id}/delete", name="member_delete_get", methods="GET")
     * @ParamConverter("member", class="App:member")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete_get(Request $request, Member $member)
    {
        return $this->render('member/delete.html.twig', array(
            'member' => $member,
        ));
    }

    /**
     * Удалить члена партии
     *
     * @Route("/{id}/delete", name="member_delete", methods="DELETE")
     * @ParamConverter("member", class="App:member")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Member $member, TokenStorageInterface $token, LoggerInterface $logger)
    {
        if ($this->isCsrfTokenValid('delete' . $member->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($member);
            $em->flush();

            //  Логируем
            $logger->info('Удален Член партии.', array(
                'ФИО' => trim($member->getLastName() . ' ' . $member->getFirstName() . ' ' . $member->getMiddleName()),
                'user' => array(
                    $token->getToken()->getUser()->getId(),
                    $token->getToken()->getUser()->getUsername(),
                    $token->getToken()->getUser()->getName(),
                ),
            ));

            //  Всплывающее сообщение
            $this->addFlash('primary', 'Удален Член партии.');
        }
        return $this->redirect($this->generateUrl('member_index'));
    }

    /**
     * Проверить анкету члена партии
     *
     * @Route("/{id}/confirm", name="member_confirm", methods="POST")
     * @ParamConverter("member", class="App:member")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function confirm(Request $request, Member $member, TokenStorageInterface $token, LoggerInterface $logger)
    {
        if ($this->isCsrfTokenValid('confirm' . $member->getId(), $request->request->get('_token'))) {
            $member->setAppStatus(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($member);
            $em->flush();

            //  Логируем
            $logger->info('Анкета Член партии - проверена.', array(
                'ФИО' => trim($member->getLastName() . ' ' . $member->getFirstName() . ' ' . $member->getMiddleName()),
                'user' => array(
                    $token->getToken()->getUser()->getId(),
                    $token->getToken()->getUser()->getUsername(),
                    $token->getToken()->getUser()->getName(),
                ),
            ));

            //  Всплывающее сообщение
            $this->addFlash('primary', 'Анкета Член партии - проверена.');
        }
        return $this->redirect($this->generateUrl('member_index'));
    }

    /**
     * Поиск Первичного отделения
     *
     * @Route("/find-po/", name="member_find_po")
     */
    public function findPo(Request $request)
    {
        $title = $request->query->get('query');
        $poList = $this->getDoctrine()->getRepository(DepartmentPo::class)->findPo($title);

        //  Результат в формате Autocomplite
        $result = [
            "query" => "Unit",
            "suggestions" => [],
        ];

        foreach ($poList as $item) {
            $result["suggestions"][] = array(
                'date' => $item->getId(),
                'value' => $item->getId() . ' # ' . $item->getTitle(),
            );
        }

        return new JsonResponse($result);
    }

    /**
     * Выдача партийных билетов. Список ЧП для выдачи
     *
     * @Route("/tickets/", defaults={"page": "1"},  name="member_tickets")
     * @Route("/tickets/page/{page}", requirements={"page": "[1-9]\d*"}, name="member_tickets_paginated")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ticketsAction(int $page, MemberRepository $repo, Request $request, TokenStorageInterface $token)
    {
        //  Постраничная разбивка
        if ($request->query->get('page')) {
            $page = $request->query->get('page');
        }

        //  Создать фильтр по МО
        $filter = $this->createForm(TicketsFilterType::class, null, [
            'action' => $this->generateUrl('member_tickets'),
            'method' => 'GET',
            'mo' => $this->getDoctrine()->getRepository(DepartmentMo::class)->findAll(),
        ]);

        //  По умолчанию МО не задан
        $moId = null;

        //  Фильтруем Членов партии
        $filter->handleRequest($request);
        if ($filter->isSubmitted() && $filter->isValid()) {
            $data = $filter->getData();

            if ($data['mo'] !== null) {
                $moId = $data['mo']->getId();
            }
        }

        //  Выбираем членов партии
        $members = $repo->findNullTickets($page, $moId);

        //  Количество присвоенных билетов
        $count = 0;

        //  Сохраняем если был POST-запрос
        if ($request->getMethod() == "POST") {
            $em = $this->getDoctrine()->getManager();

            //  Перебераем POST
            foreach ($request->request->get('form') as $id => $post) {

                //  Если номер партбилета присвоен
                if (!empty($post['partyTicket'])) {
                    //  Находим ЧП
                    $member = $repo->findOneBy(['id' => $id]);

                    //  Присваиваем номер партийного билета
                    $member->setPartyTicket($post['partyTicket']);

                    //  Присваиваем дату выдачи
                    $member->setPartyTicketDate(
                        !empty($post['partyTicketDate']) ? new \DateTime($post['partyTicketDate']) : null
                    );

                    $em->persist($member);

                    //  Итератор
                    $count++;
                }
            }

            //  Сохраняем
            $em->flush();
        }

        //  Всплывающее сообщение
        if ($count > 0) {
            $this->addFlash('primary', 'Количество присвоенных партийных билетов: ' . $count);
        }

        return $this->render('member/tickets_show.html.twig', array(
            'members' => $members,
            'filter' => $filter->createView(),
        ));
    }
}
