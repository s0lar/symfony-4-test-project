<?php

namespace App\Controller;

use App\Entity\DepartmentMo;
use App\Form\DepartmentMoType;
use App\Repository\DepartmentMoRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/department/mo")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DepartmentMoController extends Controller
{
    /**
     * @Route("/", name="department_mo_index", methods="GET")
     */
    public function index(DepartmentMoRepository $departmentMoRepository): Response
    {
        return $this->render('department_mo/index.html.twig', ['department_mos' => $departmentMoRepository->findAll()]);
    }

    /**
     * @Route("/new", name="department_mo_new", methods="GET|POST")
     */
    function new (Request $request): Response {
        $departmentMo = new DepartmentMo();
        $form = $this->createForm(DepartmentMoType::class, $departmentMo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($departmentMo);
            $em->flush();

            return $this->redirectToRoute('department_ro', ['ro' => 23]);
        }

        return $this->render('department_mo/new.html.twig', [
            'department_mo' => $departmentMo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_mo_show", methods="GET")
     */
    public function show(DepartmentMo $departmentMo): Response
    {
        return $this->render('department_mo/show.html.twig', ['department_mo' => $departmentMo]);
    }

    /**
     * @Route("/{id}/edit", name="department_mo_edit", methods="GET|POST")
     */
    public function edit(Request $request, DepartmentMo $departmentMo): Response
    {
        $form = $this->createForm(DepartmentMoType::class, $departmentMo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('department_mo_edit', ['id' => $departmentMo->getId()]);
        }

        return $this->render('department_mo/edit.html.twig', [
            'department_mo' => $departmentMo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_mo_delete", methods="DELETE")
     */
    public function delete(Request $request, DepartmentMo $departmentMo): Response
    {
        if ($this->isCsrfTokenValid('delete' . $departmentMo->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($departmentMo);
            $em->flush();
        }

        return $this->redirectToRoute('department_ro', ['ro' => 23]);
    }
}
