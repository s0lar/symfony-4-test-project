<?php

namespace App\Controller;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\DepartmentZo;
use App\Reports\Report_1;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/task")
 */
class TaskController extends Controller
{
    /**
     * @Route("/", name="task")
     */
    public function index()
    {
    }

    /**
     * Задание 1. Формируем ZIP-архив со всеми ведомостями по МО
     *
     * @Route("/report_1/{mo}/{year}/{quarter}", name="task_report1_zip")
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     * @Security("mo.isMoOwner(user)", message="Вы не можете создавать отчеты по другим МО")
     */
    public function Report1Zip(Request $request, DepartmentMo $mo, int $year, int $quarter, Report_1 $report)
    {
        //  Готовим базовые данные
        $data = [
            'quarter' => $quarter,
            'year' => $year,
        ];

        //  Массив файлов для ZIP-архива
        $files_xls = [];
        $files_zip = [];

        $listPo = $this->getDoctrine()->getManager()
            ->getRepository(DepartmentPo::class)->findByMoId($mo->getId(), ['title' => 'ASC']);

        $listZo = $this->getDoctrine()->getManager()
            ->getRepository(DepartmentZo::class)->loadZO();

        //  Сохраняем файлы ведомостей
        foreach ($listPo as $po) {
            //  Присваиваем ПО
            $data['departmentPo'] = $po;

            //  Название файла
            $filename = 'tmp/Ведомость.' . $po->getTitle() . '.xlsx';

            //  Сохраняем файл в файловой системе
            $files_xls[$po->getZoId()][] = $report->save($data, $filename);
        }

        //  Упаковываем файлы в ZIP-архив
        foreach ($files_xls as $okruk => $files) {

            //  Создаем архив
            $name = 'МО-' . $mo->getTitle() . (isset($listZo[$okruk]) ? '-' . $listZo[$okruk] : '');
            $zipname = 'tmp/' . $name . '.zip';
            $zip = new \ZipArchive;
            $zip->open($zipname, \ZipArchive::CREATE);

            //  Добавляем файлы
            foreach ($files as $file) {
                $zip->addFile($file);
            }

            //  Сохраняем ZIP-архив
            $zip->close();

            //  Сохраняем ссылки на ZIP-архивы
            $files_zip[] = [
                'name' => $name,
                'zipname' => $zipname,
            ];
        }

        return $this->render('task/report1.html.twig', [
            'files_zip' => $files_zip,
        ]);
    }
}
