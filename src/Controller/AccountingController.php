<?php

namespace App\Controller;

use App\Entity\Accounting;
use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\DepartmentRo;
use App\Entity\DepartmentZo;
use App\Entity\Member;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/accounting")
 */
class AccountingController extends Controller
{
    /**
     * Создаем записи для учета по ПО
     *
     * @Route("/new/{po}/{year}/{quarter}", name="accounting_new", methods="GET")
     * @Route("/new/{po}/")
     * @ParamConverter("po", class="App:DepartmentPo", options={"mapping": {"po": "id"}})
     * @Security("po.isMoOwner(user)", message="Вы не можете редактировать взносы других ПО")
     */
    public function new(Request $request, DepartmentPo $po, int $year = null, int $quarter = 1): Response
    {
        if (!$year) {
            $year = (int) date('Y');
        }

        //  Обновляем МО, ПО, Статус у существующих ЧП в Accounting
        $count = $this->getDoctrine()->getManager()
            ->getRepository(Accounting::class)->updateExistingsAccountings($po->getId(), $year, $quarter);

        //  Поиск новых ЧП по заданному ПО
        $newMembers = $this->getDoctrine()->getManager()
            ->getRepository(Member::class)->findNewMembersForAccounting($po->getId());

        //  Добавляем записи в учет по году и Членам партии по ПО
        $setPo = 'setPo' . $quarter;
        $setMo = 'setMo' . $quarter;
        $setStatus = 'setStatus' . $quarter;

        foreach ($newMembers as $member) {
            $accounting = new Accounting();
            $accounting->setYear($year);
            $accounting->setMember($member);

            //  Old methods
            $accounting->setPo($po->getId());
            $accounting->setMo($po->getMoId());

            //  Сохраняем данные
            $em = $this->getDoctrine()->getManager();
            $em->persist($accounting);
            $em->flush();
        }

        return $this->redirectToRoute('accounting_edit', array('po' => $po->getId(), 'year' => $year, 'quarter' => $quarter), 302);
    }

    /**
     * Вносим данные во взносам
     *
     * @Route("/edit/{po}/{year}/{quarter}", name="accounting_edit", methods="GET|POST")
     * @ParamConverter("po", class="App:DepartmentPo", options={"mapping": {"po": "id"}})
     * @Security("po.isMoOwner(user)", message="Вы не можете редактировать взносы других ПО")
     */
    public function edit(Request $request, DepartmentPo $po, int $year, int $quarter = 1): Response
    {
        //  Выбираем существующие записи для учета
        $accounting = $this->getDoctrine()->getManager()
            ->getRepository(Accounting::class)->findForAccounting($po->getId(), $year, $quarter);

        usort($accounting, function ($a, $b) {
            return strcmp($a->getMember()->getLastName(), $b->getMember()->getLastName());
        });

        //  Сохраняем
        if ($request->getMethod() == "POST") {
            //
            $em = $this->getDoctrine()->getManager();

            //  Готовим Геттер
            $getPaymentQuarter = 'getPaymentQuarter' . $quarter;

            //  Готовим Сеттеры
            $setPaymentDeclared = 'setPaymentDeclared' . $quarter;
            $setPaymentQuarter = 'setPaymentQuarter' . $quarter;
            $setDateQuarter = 'setDateQuarter' . $quarter;
            $setMoQuarter = 'setMo' . $quarter;
            $setPoQuarter = 'setPo' . $quarter;
            $setStatus = 'setStatus' . $quarter;

            //  Перебераем POST
            foreach ($request->request->get('form') as $key => $post) {

                // Заявленная сумма взноса за квартал
                if (isset($post["paymentDeclared{$quarter}"])) {

                    //  NULL = ЧП не платил!
                    $accounting[$key]->$setPaymentDeclared(
                        ($post["paymentDeclared{$quarter}"] === '') ? null : (int) $post["paymentDeclared{$quarter}"]
                    );
                }

                // Фактическая сумма взноса за квартал
                if (isset($post["paymentQuarter{$quarter}"])) {

                    //  NULL = ЧП не платил!
                    if ($post["paymentQuarter{$quarter}"] === '') {
                        $accounting[$key]->$setPaymentQuarter(null);
                    }
                    //  Если ЧП уплатил взнос, то фиксируем взнос, дату взноса, текущий МО, ПО и статус
                    else {

                        //  Но если взнос был проставлен ранее и он совпадает со взносом в POST'е
                        //  то ничего не меняем!
                        if ($accounting[$key]->$getPaymentQuarter() === (int) $post["paymentQuarter{$quarter}"]) {
                            continue;
                        }

                        // dump($accounting[$key]);
                        //  Взнос
                        $accounting[$key]->$setPaymentQuarter(
                            (int) $post["paymentQuarter{$quarter}"]
                        );

                        //  Дата взноса
                        $accounting[$key]->$setDateQuarter(
                            new \DateTime()
                        );

                        //  МО
                        $accounting[$key]->$setMoQuarter(
                            $accounting[$key]->getMember()->getMo()->getId()
                        );

                        //  ПО
                        $accounting[$key]->$setPoQuarter(
                            $accounting[$key]->getMember()->getPo()->getId()
                        );

                        //  Статус
                        $accounting[$key]->$setStatus(
                            $accounting[$key]->getMember()->getStatus()
                        );
                    }
                }

                $em->persist($accounting[$key]);
            }

            //  Сохраняем
            $em->flush();
        }

        //  Автоперенос оплат в следующий квартал
        else {
            //  Автоперенос действует со 2 квартала
            if ($quarter > 1) {

                //  Количество перенесенных оплат
                $count = 0;

                foreach ($accounting as $item) {
                    $get_declared_prev = 'getPaymentDeclared' . ($quarter - 1);
                    $get_payed_prev = 'getPaymentQuarter' . ($quarter - 1);

                    $get_declared_curr = 'getPaymentDeclared' . $quarter;
                    $set_declared_curr = 'setPaymentDeclared' . $quarter;
                    $get_payed_curr = 'getPaymentQuarter' . $quarter;
                    $set_payed_curr = 'setPaymentQuarter' . $quarter;

                    if ($item->$get_declared_prev() > 0 && $item->$get_payed_prev() > 0 &&
                        $item->$get_declared_curr() === null && $item->$get_payed_curr() === null
                    ) {
                        //  Устанавливаем заявленную сумму из предыдущего квартала
                        $item->$set_declared_curr(
                            $item->$get_declared_prev()
                        );

                        //  Устанавливаем оплаченную сумму из предыдущего квартала
                        $item->$set_payed_curr(
                            $item->$get_payed_prev()
                        );

                        $count++;
                    }
                }

                if ($count > 0) {
                    $this->addFlash('primary', "Количество оплат перенесенных в новый квартал = {$count}. Нажмите кнопку СОХРАНИТЬ для завершения переноса!");
                }
            }
        }

        return $this->render('accounting/edit.html.twig', [
            'po' => $po,
            'year' => $year,
            'quarter' => $quarter,
            'accounting' => $accounting,
        ]);
    }

    /**
     * Выбираем отделение, для учета
     *
     * @Route("/", name="accounting_index", methods="GET")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('accounting_ro', array('ro' => 23), 302);
    }

    /**
     * Выбираем местное отделение, для учета
     *
     * @Route("/ro/{ro}/", name="accounting_ro", methods="GET")
     * @ParamConverter("ro", class="App:DepartmentRo", options={"mapping": {"ro": "id"}})
     */
    public function index_ro(DepartmentRo $ro, TokenStorageInterface $token): Response
    {
        //  Получаем текущего пользователя
        $user = $token->getToken()->getUser();

        //  Если роль пользователя МИК, то перенаправляем сразу на заданный МО
        if (in_array('ROLE_USER', $user->getRoles()) && $user->getDepartmentMo()) {
            return $this->redirectToRoute('accounting_mo', [
                'mo' => $user->getDepartmentMo()->getId(),
            ]);
        }

        //  Показываем список МО
        $list = $this->getDoctrine()->getManager()->getRepository(DepartmentMo::class)->findByRoId($ro->getId());
        return $this->render('accounting/index_mo.html.twig', [
            'mo' => $list,
        ]);
    }

    /**
     * Выбираем первичное отделение, для учета
     *
     * @Route("/mo/{mo}/", name="accounting_mo", methods="GET")
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     * @Security("mo.isMoOwner(user)", message="Вы не можете редактировать взносы других МО")
     */
    public function index_mo(DepartmentMo $mo): Response
    {
        $list = $this->getDoctrine()->getManager()->getRepository(DepartmentPo::class)->findByMoId($mo->getId(), ['title' => 'ASC']);
        $zo = $this->getDoctrine()->getManager()->getRepository(DepartmentZo::class)->findAll();

        return $this->render('accounting/index_po.html.twig', [
            'mo' => $mo,
            'po' => $list,
            'zo' => $zo,
        ]);
    }

    /**
     * Перенаправляем на внос данных
     *
     * @Route("/po/{po}/", name="accounting_po", methods="GET")
     * @ParamConverter("po", class="App:DepartmentPo", options={"mapping": {"po": "id"}})
     * @Security("po.isMoOwner(user)", message="Вы не можете редактировать взносы других ПО")
     */
    public function index_po(DepartmentPo $po): Response
    {
        $year = date('Y');
        return $this->redirectToRoute('accounting_new', array('po' => $po->getId(), 'year' => $year), 302);
    }
}
