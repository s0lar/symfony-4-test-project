<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserCreateType;
use App\Form\UserUpdateType;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Контроллер управления пользователями
 *
 * @Route("/user")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends Controller
{
    /**
     * Показать всех пользователей
     *
     * @Route("/", name="user_index")
     */
    public function index()
    {
        //  Выбрать всех пользователей
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('user/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Создания нового пользователя
     *
     * @Route("/create", name="user_create")
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, LoggerInterface $logger)
    {
        //  Создать Entity
        $user = new User();

        //  Созлать форму
        $form = $this->createForm(UserCreateType::class, $user, [
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //  Устанавливаем username = email
            $user->setUsername($user->getEmail());

            //  Генерируем пароль
            $pass = substr(bin2hex(random_bytes(32)), 0, 8);

            //  Хешируем пароль
            $encoded = $encoder->encodePassword($user, $pass);
            $user->setPassword($encoded);

            //  Указываем дату создания
            $user->setCreatedAt(new \DateTime());

            //  Указываем дату обновления
            $user->setUpdatedAt(new \DateTime());

            //  Активируем учетную запись
            $user->setIsActive(true);

            //  Отправляем сообщение на e-mail
            $message = (new \Swift_Message('Новый пользователь'))
                ->setFrom($this->container->getParameter('mailFrom'))
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'emails/userCreate.html.twig',
                        array(
                            'user' => $user,
                            'pass' => $pass,
                        )
                    ),
                    'text/html'
                );
            $res = $mailer->send($message);

            //  Сохраняем данные
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            //  Логируем действия
            $logger->info('Новый пользователь создан (' . $user->getUsername() . ')');

            //  Всплывающее сообщение
            $this->addFlash('primary', 'Новый пользователь создан');

            //  Редирект
            return $this->redirect($this->generateUrl('user_index'));
        }

        return $this->render('user/create.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Показать выбранного пользователя
     *
     * @Route("/{id}", name="user_read")
     * @Method("GET")
     * @ParamConverter("user", class="App:user")
     */
    public function read(User $user)
    {
        //  Создать форму
        $form = $this->createForm(UserUpdateType::class, $user, [
            'action' => $this->generateUrl('user_update', ['id' => $user->getId()]),
            'method' => 'POST',
        ]);

        return $this->render('user/read.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Изменить пользователя
     *
     * @Route("/{id}", name="user_update")
     * @Method("POST")
     * @ParamConverter("user", class="App:user")
     */
    public function update(Request $request, User $user, LoggerInterface $logger)
    {
        //  Создать форму
        $form = $this->createForm(UserUpdateType::class, $user, [
            'action' => $this->generateUrl('user_update', ['id' => $user->getId()]),
            'method' => 'POST',
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //  Указываем дату обновления
            $user->setUpdatedAt(new \DateTime());

            //  Сохраняем данные
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            //  Логируем действия
            $logger->info('Пользователь "' . $user->getUsername() . '" отредактирован.');
        }

        return $this->render('user/read.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }
}
