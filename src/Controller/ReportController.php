<?php

namespace App\Controller;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\Member;
use App\Form\ReportFilter1Type;
use App\Form\ReportFilter2Type;
use App\Form\ReportFilter3Type;
use App\Form\ReportFilter4Type;
use App\Form\ReportFilter5Type;
use App\Reports\Report_1;
use App\Reports\Report_2;
use App\Reports\Report_3;
use App\Reports\Report_4;
use App\Reports\Report_5;
use App\Reports\Report_9;
use App\Reports\Report_10;
use App\Reports\Report_11;
use App\Reports\Report_12;
use App\Reports\Report_41;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/report")
 */
class ReportController extends Controller
{
    /**
     * @Route("/", name="report")
     */
    public function index()
    {
        return $this->render('report/index.html.twig', [
            'controller_name' => 'ReportController',
        ]);
    }

    ###########
    # Отчет 1 #
    ###########

    /**
     * Отчет 1. Выбор отделения и периода
     *
     * @Route("/report_1/", name="report1_filter")
     */
    public function report1_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter1Type::class, null, [
            'action' => $this->generateUrl('report1_filter'),
            'department_mo' => $this->getDoctrine()->getRepository(DepartmentMo::class)->loadMO(),
        ]);

        //  Создать форму
        $form2 = $this->createForm(ReportFilter2Type::class, null, [
            'action' => $this->generateUrl('report1_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report1_xls',
                [
                    'po' => $data['po']->getId(),
                    'year' => $data['year'],
                    'quarter' => $data['quarter'],
                ]
            );
        }

        //  Обработка запроса
        $form2->handleRequest($request);
        if ($form2->isSubmitted() && $form2->isValid()) {
            $data = $form2->getData();

            return $this->redirectToRoute(
                'task_report1_zip',
                [
                    'mo' => $data['mo']->getId(),
                    'year' => $data['year'],
                    'quarter' => $data['quarter'],
                ]
            );
        }

        return $this->render('report/report1_filter.html.twig', [
            'form' => $form->createView(),
            'form2' => $form2->createView(),
        ]);
    }

    /**
     * Отчет 1. Ведомость по уплате взносов за квартал
     *
     * @Route("/report_1/{po}/{year}/{quarter}/", name="report1_xls")
     * @ParamConverter("po", class="App:DepartmentPo", options={"mapping": {"po": "id"}})
     * @Security("po.isMoOwner(user)", message="Вы не можете создавать отчеты по другим ПО")
     */
    public function report1_xls(Request $request, DepartmentPo $po, int $year, int $quarter, Report_1 $report)
    {
        //  Готовим базовые данные
        $data = [
            'departmentPo' => $po,
            'quarter' => $quarter,
            'year' => $year,
        ];

        //  Название файла
        $filename = 'tmp/Ведомость. ' . $po->getTitle() . '.xlsx';

        //  Генерируем отчет и сохраняем
        $output = $report->save($data, $filename);

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ###########
    # Отчет 2 #
    ###########

    /**
     * Отчет 2. Выбор отделения и периода
     *
     * @Route("/report_2/", name="report2_filter")
     */
    public function report2_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter2Type::class, null, [
            'action' => $this->generateUrl('report2_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report2_xls',
                [
                    'mo' => $data['mo']->getId(),
                    'year' => $data['year'],
                    'quarter' => $data['quarter'],
                ]
            );
        }

        return $this->render('report/report2_filter.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Отчет 2. Отчет об уплате взносов за период по Муниципальному отделению
     *
     * @Route("/report_2/{mo}/{year}/{quarter}", name="report2_xls")
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     * @Security("mo.isMoOwner(user)", message="Вы не можете создавать отчеты по другим МО")
     */
    public function report2_xls(Request $request, DepartmentMo $mo, int $year, int $quarter, Report_2 $report)
    {
        //  Шапка отчета
        $data['period'] = "Отчет об уплате взносов за {$quarter} кв. {$year} г.";
        $data['quarter'] = $quarter;
        $data['year'] = $year;
        $data['ro_title'] = 'Краснодарское';
        $data['mo_title'] = $mo->getTitle();
        $data['mo'] = $mo;

        //  Генерируем отчет и сохраняем
        // $report = new Report_2();
        $output = $report->save($data, 'tmp/report_2.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ###########
    # Отчет 3 #
    ###########

    /**
     * Отчет 3. Реестр местных отделений Партии "ЕДИНАЯ РОССИЯ"
     *
     * @Route("/report_3/", name="report3_filter")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report3_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter3Type::class, null, [
            'action' => $this->generateUrl('report3_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report3_xls', [
                    'date' => $data['date']->format('Y-m-d'),
                ]
            );
        }

        return $this->render('report/report3_filter.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * Отчет 3. Ведомость по уплате взносов по Первичным отделениям за квартал
     *
     * @Route("/report_3/{date}", name="report3_xls")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report3_xls(Request $request, \DateTime $date, Report_3 $report)
    {
        //  Подготавливаем полученные данные
        $data = [
            'date' => $date,
        ];

        //  Сохраняем отчет
        $output = $report->save($data, 'tmp/report_3.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ###########
    # Отчет 4 #
    ###########

    /**
     * Отчет 4. Прием в партию за год
     *
     * @Route("/report_4/", name="report4_filter")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report4_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter3Type::class, null, [
            'action' => $this->generateUrl('report4_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report4_xls',
                [
                    'date' => $data['date']->format('Y-m-d'),
                ]
            );
        }

        return $this->render('report/report4_filter.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Отчет 4. Прием в партию за год по МО
     *
     * @Route("/report_4/{date}", name="report4_xls")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report4_xls(Request $request, \DateTime $date, Report_4 $report)
    {
        //  Подготавливаем полученные данные
        $data = [
            'date' => $date,
        ];

        //  Сохраняем отчет
        $output = $report->save($data, 'tmp/report_4.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ############
    # Отчет 41 #
    ############

    /**
     * Отчет 4.1. Прием в партию за год по ПО
     *
     * @Route("/report_41/", name="report41_filter")
     */
    public function report41_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter4Type::class, null, [
            'action' => $this->generateUrl('report41_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report41_xls',
                [
                    'date' => $data['date']->format('Y-m-d'),
                    'mo' => $data['mo']->getId(),
                ]
            );
        }

        return $this->render('report/report41_filter.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Отчет 4.1. Прием в партию за год по ПО
     *
     * @Route("/report_41/{date}/{mo}/", name="report41_xls")
     * @Security("mo.isMoOwner(user)", message="Вы не можете создавать отчеты по другим МО")
     */
    public function report41_xls(Request $request, \DateTime $date, DepartmentMo $mo = null, Report_41 $report)
    {
        //  Подготавливаем полученные данные
        $data = [
            'date' => $date,
            'mo' => $mo,
        ];

        //  Сохраняем отчет
        $output = $report->save($data, 'tmp/report_41.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ###########
    # Отчет 5 #
    ###########

    /**
     * Отчет 5. Количественный и качественный состав членов Партии
     *
     * @Route("/report_5/", name="report5_filter")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report5_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter3Type::class, null, [
            'action' => $this->generateUrl('report5_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report5_xls',
                [
                    'date' => $data['date']->format('Y-m-d'),
                ]
            );
        }

        return $this->render('report/report5_filter.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Отчет 5. Количественный и качественный состав членов Партии
     *
     * @Route("/report_5/{date}", name="report5_xls")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report5_xls(Request $request, \DateTime $date, Report_5 $report)
    {
        //  Подготавливаем полученные данные
        $data = [
            'date' => $date,
        ];

        //  Сохраняем отчет
        $output = $report->save($data, 'tmp/report_5.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ###########
    # Отчет 9 #
    ###########

    /**
     * Отчет 9. Выбор отделения и периода
     *
     * @Route("/report_9/", name="report9_filter")
     */
    public function report9_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter2Type::class, null, [
            'action' => $this->generateUrl('report9_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report9_xls',
                [
                    'mo' => $data['mo']->getId(),
                    'year' => $data['year'],
                    'quarter' => $data['quarter'],
                ]
            );
        }

        return $this->render('report/report9_filter.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * Отчет 9. Ведомость по уплате взносов по Первичным отделениям за квартал
     *
     * @Route("/report_9/{mo}/{year}/{quarter}", name="report9_xls")
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     * @Security("mo.isMoOwner(user)", message="Вы не можете создавать отчеты по другим МО")
     */
    public function report9_xls(Request $request, DepartmentMo $mo, int $year, int $quarter, Report_9 $report)
    {
        //  Подготавливаем полученные данные
        $data = [
            'mo' => $mo,
            'year' => $year,
            'quarter' => $quarter,
        ];

        //  Сохраняем отчет
        $output = $report->save($data, 'tmp/report_9.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ############
    # Отчет 10 #
    ############

    /**
     * Отчет 10. Сверка уплаты по всем членам партии за период по Местному отделению
     *
     * @Route("/report_10/", name="report10_filter")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report10_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter2Type::class, null, [
            'action' => $this->generateUrl('report10_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report10_xls',
                [
                    'mo' => $data['mo']->getId(),
                    'year' => $data['year'],
                    'quarter' => $data['quarter'],
                ]
            );

        }

        return $this->render('report/report10_filter.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Отчет 10. Сверка уплаты по всем членам партии за период по Местному отделению
     *
     * @Route("/report_10/{mo}/{year}/{quarter}", name="report10_xls")
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report10_xls(Request $request, DepartmentMo $mo, int $year, int $quarter, Report_10 $report)
    {
        //  Подготавливаем полученные данные
        $data = [
            'mo' => $mo,
            'year' => $year,
            'quarter' => $quarter,
        ];

        //  Сохраняем отчет
        $output = $report->save($data, 'tmp/report_10.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ##################
    # Отчет - Сверка #
    ##################

    /**
     * @Route("/report/revise/", name="report_revise")
     */
    public function index_revise()
    {
        $mo = $this->getDoctrine()->getRepository(DepartmentMo::class)->findAll();

        return $this->render('report/revise.html.twig', [
            'mo' => $mo,
        ]);
    }

    ############
    # Отчет 11 #
    ############

    /**
     * Отчет 11. Выгрузка членов партии по фильтрам
     *
     * @Route("/report_11/", name="report11_xls")
     */
    public function report11_xls($filter, Report_11 $report)
    {
        //  Подготавливаем полученные данные
        $data = [
            'filter' => $filter,
        ];

        //  Сохраняем отчет
        $output = $report->save($data, 'tmp/report_11.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    /**
     * @Route("/report/revise/mo/{mo}/", name="report_revise_mo")
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     */
    public function index_revise_mo(DepartmentMo $mo)
    {
        //
        $po = $this->getDoctrine()->getRepository(DepartmentPo::class)->findByMoId($mo->getId());

        return $this->render('report/revise.html.twig', [
            'po' => $po,
        ]);
    }

    /**
     * @Route("/report/revise/po/{po}/", name="report_revise_po")
     * @ParamConverter("po", class="App:DepartmentPo", options={"mapping": {"po": "id"}})
     */
    public function index_revise_po(DepartmentPo $po)
    {
        //
        $members = $this->getDoctrine()->getRepository(Member::class)->findByPo($po, array('lastName' => 'ASC'));

        $file = "tmp/revise_" . $po->getId() . ".xlsx";
        $row = 1;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        //  Заголовок
        $sheet->setCellValue('A' . $row, 'Сверка Членов партии по Первичному отделению №' . $po->getId() . ' ' . $po->getTitle());
        $row += 2;

        $styleHeader = [
            'font' => [
                'bold' => true,
                'size' => 13,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        $styleRow = [
            'font' => [
                'size' => 12,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        //  Стилизуем заголовок
        $sheet->getStyle('A1:G' . $row)->applyFromArray($styleHeader);

        $sheet->getColumnDimension('A')->setWidth(18);
        $sheet->getColumnDimension('B')->setWidth(14);
        $sheet->getColumnDimension('C')->setWidth(16);
        $sheet->getColumnDimension('D')->setWidth(12);
        $sheet->getColumnDimension('E')->setWidth(18);
        $sheet->getColumnDimension('F')->setWidth(16);
        $sheet->getColumnDimension('G')->setWidth(12);

        //  Шапка таблицы
        $sheet->setCellValue('A' . $row, 'Фамилия');
        $sheet->setCellValue('B' . $row, 'Имя');
        $sheet->setCellValue('C' . $row, 'Отчество');
        $sheet->setCellValue('D' . $row, 'Дата рождения');
        $sheet->setCellValue('E' . $row, 'Статус');
        $sheet->setCellValue('F' . $row, 'Паспорт');
        $sheet->setCellValue('G' . $row, '№ партбилета');

        foreach ($members as $member) {
            $row++;
            $sheet->setCellValue('A' . $row, $member->getLastName());
            $sheet->setCellValue('B' . $row, $member->getFirstName());
            $sheet->setCellValue('C' . $row, $member->getMiddleName());
            $sheet->setCellValue('D' . $row, $member->getBirthday()->format('d-m-Y'));
            $sheet->setCellValue('E' . $row, $member->getStatus(true));
            $sheet->setCellValue('F' . $row, $member->getPassportSeries() . '    ' . $member->getPassportNumber());
            $sheet->setCellValue('G' . $row, ' ' . $member->getPartyTicket());

            //  Стилизуем
            $sheet->getStyle("A{$row}:G{$row}")->applyFromArray($styleRow);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);

        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    ############
    # Отчет 12 #
    ############

    /**
     * Отчет 12. Ведомость выдачи партийных билетов
     *
     * @Route("/report_12/", name="report12_filter")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report12_filter(Request $request)
    {
        //  Создать форму
        $form = $this->createForm(ReportFilter5Type::class, null, [
            'action' => $this->generateUrl('report12_filter'),
        ]);

        //  Обработка запроса
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                'report12_xls',
                [
                    'mo' => $data['mo']->getId(),
                    'date' => $data['date']->format('Y.m.d'),
                ]
            );

        }

        return $this->render('report/report12_filter.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Отчет 12. Ведомость выдачи партийных билетов
     *
     * @Route("/report_12/{mo}/{date}", name="report12_xls")
     * @ParamConverter("mo", class="App:DepartmentMo", options={"mapping": {"mo": "id"}})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function report12_xls(Request $request, DepartmentMo $mo, string $date, Report_12 $report)
    {
        //  Подготавливаем полученные данные
        $data = [
            'mo' => $mo,
            'date' => $date,
        ];

        //  Сохраняем отчет
        $output = $report->save($data, 'tmp/report_12.xlsx');

        //  Отдаем отчет для скачивания
        $response = new BinaryFileResponse($output);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }
}
