<?php

namespace App\Repository;

use App\Entity\Member;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

class MemberRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Member::class);
    }

    /**
     * Создаем постраничную разбивку
     *
     * @param Query $query
     * @param integer $page
     * @return Pagerfanta
     */
    private function createPaginator($query, int $page): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage(Member::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    /**
     * Найти всех партийцев с постраничной разбивкой
     *
     * @param integer $page
     * @param array $param
     * @return Pagerfanta
     */
    public function findMembers(int $page = 1, array $param = null, $with_paginator = true)
    {
        $query = $this->createQueryBuilder('m');

        if ($param) {
            //  MO
            if (!empty($param['mo'])) {
                $query->andWhere("m.mo = :mo")->setParameter("mo", $param['mo']);
            }

            //  PO
            if (!empty($param['po'])) {
                $query->andWhere("m.po = :po")->setParameter("po", $param['po']);
            }

            //  status
            if (!empty($param['status'])) {
                $query->andWhere("m.status = :status")->setParameter("status", $param['status']);
            }

            //  lastName
            if (!empty($param['last_name'])) {
                $query->andWhere("m.lastName LIKE :last_name")->setParameter("last_name", "%" . $param['last_name'] . "%");
            }

            //  firstName
            if (!empty($param['first_name'])) {
                $query->andWhere("m.firstName LIKE :first_name")->setParameter("first_name", "%" . $param['first_name'] . "%");
            }

            //  middleName
            if (!empty($param['middle_name'])) {
                $query->andWhere("m.middleName LIKE :middle_name")->setParameter("middle_name", "%" . $param['middle_name'] . "%");
            }

            //  partyTicket
            if (!empty($param['party_ticket'])) {
                $query->andWhere("m.partyTicket LIKE :party_ticket")->setParameter("party_ticket", "%" . $param['party_ticket'] . "%");
            }

            //  passport_number
            if (!empty($param['passport_number'])) {
                $query->andWhere("m.passportNumber LIKE :passport_number")->setParameter("passport_number", "%" . $param['passport_number'] . "%");
            }

            //  Парсим дату для поиска дней рождений
            if (!empty($param['birthday'])) {
                $birthday = date_parse_from_format('d.m.Y', $param['birthday']);

                if (isset($birthday['day']) && $birthday['day']) {
                    $query->andWhere('DAY(m.birthday) = :birthday_day')->setParameter("birthday_day", $birthday['day']);
                }

                if (isset($birthday['month']) && $birthday['month']) {
                    $query->andWhere('MONTH(m.birthday) = :birthday_month')->setParameter("birthday_month", $birthday['month']);
                }

                if (isset($birthday['year']) && $birthday['year']) {
                    $query->andWhere('YEAR(m.birthday) = :birthday_year')->setParameter("birthday_year", $birthday['year']);
                }
            }

            //  Определяем сортировку
            if (isset($param['sort_field']) && in_array($param['sort_field'], ['lastName', 'firstName', 'middleName', 'partyTicket', 'status', 'birthday', 'sex', 'acceptedDate'])) {
                $sortBy = $param['sort_field'];
            } else {
                $sortBy = 'appStatus';
            }

            //  Направление сортировки
            $sortDir = (isset($param['sort_type']) && $param['sort_type'] == 'DESC') ? 'DESC' : 'ASC';

            $query->orderBy('m.' . $sortBy, $sortDir);
        } else {
            $query->orderBy('m.appStatus', 'ASC');
        }

        //  Возвращаем запрос для пагинации
        if ($with_paginator) {
            return $this->createPaginator($query, $page);
        }
        //  Возвращаем членов партии для выгрузки XLS
        else {
            return $query->getQuery()->getResult();
        }
    }

    /**
     * Найти всех партийцев без партийных билетов
     *
     * @param integer $page
     * @return Pagerfanta
     */
    public function findNullTickets(int $page = 1, $mo = null)
    {
        $query = $this->createQueryBuilder('m');
        //  Фильтруем по МО, если задано
        if ($mo) {
            $query->andWhere('m.mo = :mo')->setParameter('mo', $mo);
        }
        $query->andWhere("m.partyTicket IS NULL");
        $query->orderBy('m.lastName', 'ASC');

        return $this->createPaginator($query, $page);
    }

    /**
     * Выбираем только подтвержденных и действующих партийцев для учета взносов.
     *
     * @param integer $po
     * @param array $existing
     * @return array
     */
    public function findForAccounting(int $po, array $existing)
    {
        return $query = $this->createQueryBuilder('m', 'm.id')
            ->where('m.po = :po')->setParameter('po', $po)
            ->andWhere('m.id NOT in (:existing)')->setParameter('existing', $existing)
            ->andWhere('m.status = 3')
            ->orderBy('m.lastName', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Выбираем членов партии для квартальной ведомости.
     * Report 1
     *
     * @param integer $po
     * @param array $existing
     * @return array
     */
    public function findForReport1(int $po)
    {
        return $query = $this->createQueryBuilder('m')
            ->select('
                m.id, CONCAT(m.lastName, \' \', SUBSTRING(m.firstName, 1, 1), \'.\', SUBSTRING(m.middleName, 1, 1), \'.\') as name, m.partyTicket as ticket,
                a.paymentDeclared1,
                a.paymentDeclared2,
                a.paymentDeclared3,
                a.paymentDeclared4,
                a.paymentQuarter1,
                a.paymentQuarter2,
                a.paymentQuarter3,
                a.paymentQuarter4
            ')
            ->leftJoin(
                'App\Entity\Accounting',
                'a',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'a.member = m.id'
            )
            ->where('m.po = :po')->setParameter('po', $po)
            ->andWhere('m.status = 3')
            ->orderBy('m.lastName', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Количество членов партии за период
     * Report 2
     *
     * @param integer $mo
     * @param array $existing
     * @return array
     */
    public function membersCount(int $mo, int $year, int $quarter)
    {
        //  Создаем дату по кварталу и году
        $month = $quarter * 3;
        $date = new \DateTime("{$year}-{$month}-01");
        $date->modify('+1 month');
        $date->modify('-1 day');

        return $query = $this->createQueryBuilder('m')
            ->select('COUNT(m.id) as count')
            ->where('m.mo = :mo')->setParameter('mo', $mo)
            ->andWhere('m.acceptedDate <= :date or m.partyTicketDate <= :date')->setParameter('date', $date)
            ->andWhere('m.status = 3')
            ->orderBy('m.lastName', 'ASC')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Количество членов по Первичный отделениям
     * Report 9
     *
     * @param integer $mo
     * @param array $existing
     * @return array
     */
    public function membersCountByPo(int $mo, int $year, int $quarter)
    {
        //  Создаем дату по кварталу и году
        $month = $quarter * 3;
        $date = new \DateTime("{$year}-{$month}-01");
        $date->modify('+1 month');
        $date->modify('-1 day');

        $query = $this->createQueryBuilder('m')
            ->select('COUNT(m.id) as count, IDENTITY(m.po) as po')
            ->where('m.mo = :mo')->setParameter('mo', $mo)
            ->andWhere('m.acceptedDate <= :date')->setParameter('date', $date)
            ->andWhere('m.status = 3')
            ->groupBy('m.po')
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['po']] = $item['count'];
        }

        return $result;
    }

    /**
     * Количество принятых членов партии по Местным отделениям
     * Report 4
     *
     * @return array
     */
    public function membersCountByMo(\DateTime $date)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m.id) as count, IDENTITY(m.mo) as id')
            ->andWhere('m.acceptedDate <= :date')->setParameter('date', $date)
            ->andWhere('m.status = 3')
            ->groupBy('m.mo');

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['id']] = $item['count'];
        }

        return $result;
    }

    /**
     * Количество принятых членов партии по Первичным отделениям
     * Report 4.1
     *
     * @return array
     */
    public function membersAcceptedCountByPo(int $mo, \DateTime $date, bool $is_accepted = false)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m.id) as count, IDENTITY(m.po) as id')
            ->andWhere('m.mo = :mo')->setParameter('mo', $mo)
            ->andWhere('m.acceptedDate <= :date')->setParameter('date', $date)
            ->andWhere('m.status = 3')
            ->groupBy('m.po');

        if ($is_accepted) {
            $query->andWhere('m.acceptedDate <= :date')->setParameter('date', $date);
        }

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['id']] = $item['count'];
        }

        return $result;
    }

    /**
     * Количество членов по Местным отделениям
     * Report 3
     *
     * @return array
     */
    public function membersInCountByMo(\DateTime $date, bool $is_accepted = false)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m.id) as count, IDENTITY(m.mo) as id')
            ->where('m.status = 3');

        if ($is_accepted) {
            $query
                ->andWhere('m.acceptedDate <= :date')->setParameter('date', $date)
                ->andWhere('m.partyTicket != :ticket')->setParameter('ticket', '');
        }

        $query->groupBy('m.mo');

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['id']] = $item['count'];
        }

        return $result;
    }

    /**
     * Количество членов по Местным отделениям за интервал
     * Report 4
     *
     * @return array
     */
    public function membersIntervalCountByMo(\DateTime $date_from, \DateTime $date_to)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m.id) as count, IDENTITY(m.mo) as id')
            ->andWhere('m.acceptedDate < :date_to')->setParameter('date_to', $date_to)
            ->andWhere('m.acceptedDate >= :date_from')->setParameter('date_from', $date_from)
            ->andWhere('m.status = 3')
            ->groupBy('m.mo');

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['id']] = $item['count'];
        }

        return $result;
    }

    /**
     * Количество членов по Первичным отделениям за интервал
     * Report 4.1
     *
     * @return array
     */
    public function membersIntervalCountByPo(int $mo, \DateTime $date_from, \DateTime $date_to)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m.id) as count, IDENTITY(m.po) as id')
            ->andWhere('m.mo = :mo')->setParameter('mo', $mo)
            ->andWhere('m.acceptedDate < :date_to')->setParameter('date_to', $date_to)
            ->andWhere('m.acceptedDate >= :date_from')->setParameter('date_from', $date_from)
            ->andWhere('m.status = 3')
            ->groupBy('m.po');

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['id']] = $item['count'];
        }

        return $result;
    }

    /**
     * Кол-во членов партии по Полу
     * Report 5
     *
     * @return array
     */
    public function membersCountBySex(\DateTime $date)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('m.sex, COUNT(m.id) as count')
            ->andWhere('m.status = 3')
            ->groupBy('m.sex');

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['sex']] = $item['count'];
        }

        return $result;
    }

    /**
     * Кол-во членов партии по Образованию
     * Report 5
     *
     * @return array
     */
    public function membersCountByEducation(\DateTime $date)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('m.education, COUNT(m.id) as count')
            // ->andWhere('m.acceptedDate <= :date')->setParameter('date', $date)
            // ->andWhere('m.status = 3 OR m.status IS NULL')
            ->andWhere('m.status = 3')
            ->groupBy('m.education');

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['education']] = $item['count'];
        }

        return $result;
    }

    /**
     * Кол-во членов партии по Возрасту
     * Report 5
     *
     * @return array
     */
    public function membersCountByAge(\DateTime $today, $age_from, $age_to)
    {
        $day_from = clone $today;
        $day_to = clone $today;

        $day_from = $day_from->modify("-$age_from years");
        $day_to = $day_to->modify("-$age_to years")->modify("+1 day");

        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m.id) as count')
            ->andWhere('m.birthday < :day_from')->setParameter('day_from', $day_from)
            ->andWhere('m.birthday >= :day_to')->setParameter('day_to', $day_to)
            ->andWhere('m.status = 3');

        return $query = $query->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Кол-во членов партии по Социальной категории
     * Report 5
     *
     * @return array
     */
    public function membersCountBySocial(\DateTime $date)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('m.socialCategory, COUNT(m.id) as count')
            ->andWhere('m.status = 3')
            ->groupBy('m.socialCategory');

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['socialCategory']] = $item['count'];
        }

        return $result;
    }

    /**
     * Кол-во членов партии по Сфере деятельности
     * Report 5
     *
     * @return array
     */
    public function membersCountByWork(\DateTime $date)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('m.workSphere, COUNT(m.id) as count')
            ->andWhere('m.status = 3')
            ->groupBy('m.workSphere');

        $query = $query->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['workSphere']] = $item['count'];
        }

        return $result;
    }

    /**
     * Кол-во членов партии с выданными партбилетами
     * Report 5
     *
     * @return array
     */
    public function membersCountByPartyTicket()
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m.id) as count')
            ->andWhere('m.partyTicket != :ticket')->setParameter('ticket', '')
            ->andWhere('m.status = 3');

        return $query = $query->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Кол-во членов партии
     * Report 5
     *
     * @return array
     */
    public function membersCountTotal()
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m.id) as count')
            ->andWhere('m.status = 3');

        return $query = $query->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Сверка уплаты по всем членам партии за период по Местному отделению
     * Report 10
     *
     * @return array
     */
    public function allMembersJoinAccounting($mo, $year)
    {
        $query = $this->createQueryBuilder('m', 'm.id');

        $query->select('
                m.id,
                CONCAT(m.lastName, \' \', SUBSTRING(m.firstName, 1, 1), \'.\', SUBSTRING(m.middleName, 1, 1), \'.\') as name,
                m.partyTicket as ticket,
                m.status,
                m.acceptedDate,
                m.partyExitDate
            ')
            ->where('m.mo = :mo')->setParameter('mo', $mo);

        return $query = $query->getQuery()
            ->getResult();
    }

    /**
     * @param integer $po
     * @return void
     */
    public function findNewMembersForAccounting(int $po)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->leftJoin('App\Entity\Accounting', 'a', 'WITH', 'm.id = a.member')
            ->andWhere('m.po = :po')->setParameter('po', $po)
            ->andWhere('m.status = 3')
            ->andWhere('a.member IS NULL');

        return $query = $query->getQuery()
            ->getResult();
    }

    /**
     * Ищем несоответствие МО и ПО
     *
     * @param integer $mo
     * @param integer $po
     * @return void
     */
    public function findWrongMoPo(int $mo, int $po)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->where('m.mo != :mo and m.po = :po')
            // ->orWhere('m.mo != :mo and m.po = :po')
            ->setParameter('mo', $mo)
            ->setParameter('po', $po);

        return $query = $query->getQuery()
            ->getResult();
    }

    /**
     * Выбираем членов партии без партийных номеров, для присвоения
     *
     * @return array
     */
    public function findNullPartyTickets()
    {
        return $query = $this->createQueryBuilder('m')
            ->select('
                m.id,
                CONCAT(m.lastName, \' \', SUBSTRING(m.firstName, 1, 1), \'.\', SUBSTRING(m.middleName, 1, 1), \'.\') as name,
                m.partyTicket,
                mo.mo
            ')
            ->leftJoin(
                'App\Entity\DepartmentMo',
                'mo',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'mo.id = m.mo'
            )
            ->where('m.partyTicket  is NULL')
            ->orderBy('m.lastName', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
