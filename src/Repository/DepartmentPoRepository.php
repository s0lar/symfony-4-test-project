<?php

namespace App\Repository;

use App\Entity\DepartmentPo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DepartmentPoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DepartmentPo::class);
    }

    public function findPo($title)
    {
        return $this->createQueryBuilder('d')
            ->where('d.title like :title')
            ->setParameter('title', "%$title%")
            ->getQuery()
            ->getResult();
    }

    /**
     * Количество Первичных отделений по Муниципальным отделениям
     * Report 4
     *
     * @return array
     */
    public function poCountByMo()
    {
        $query = $this->createQueryBuilder('m')
            ->select('m.moId, COUNT(m.id) as count')
            ->groupBy('m.moId')
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['moId']] = $item['count'];
        }

        return $result;
    }

    /**
     * Кол-во ПО
     *
     * @return array
     */
    public function countPo()
    {
        $query = $this->createQueryBuilder('m');
        $query->select('COUNT(m.id) as count');

        return $query->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Названия первичных отделений
     *
     * @return array
     */
    public function poTitleByMo($mo)
    {
        $query = $this->createQueryBuilder('m')
            ->select('m.id, m.title')
            ->where('m.moId = :moId')->setParameter('moId', $mo)
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($query as $item) {
            $result[$item['id']] = $item['title'];
        }

        return $result;
    }
}
