<?php

namespace App\Repository;

use App\Entity\Accounting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AccountingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Accounting::class);
    }

    /**
     * Выбираем ID партийцев заданные для учета по периоду и ПО
     *
     * @param integer $po
     * @param integer $year
     * @param integer $quarter
     * @return array()
     */
    public function findForExisting(int $po, int $year, int $quarter = 1)
    {
        $result = [];
        $query = $this->createQueryBuilder('m')
            ->where("m.po{$quarter} = :po")->setParameter('po', $po)
            ->andWhere('m.year = :year')->setParameter('year', $year)
            ->getQuery()
            ->getResult();

        foreach ($query as $item) {
            $result[$item->getMember()->getId()] = $item->getMember()->getId();
        }

        return $result;
    }

    /**
     * Выбираем записи партийцев для учета по периоду и ПО
     *
     * @param integer $po
     * @param integer $year
     * @param integer $quarter
     * @return array()
     */
    public function findForAccounting(int $po, int $year, int $quarter)
    {
        return $query = $this->createQueryBuilder('m')
        // ->where("m.po = :po OR m.po1 = :po OR m.po2 = :po OR m.po3 = :po OR m.po4 = :po")->setParameter('po', $po)
            ->where("m.po = :po OR m.po{$quarter} = :po")->setParameter('po', $po)
            ->andWhere('m.year = :year')->setParameter('year', $year)
            ->getQuery()
            ->getResult();
    }

    /**
     * Выбираем записи партийцев для учета по периоду и МО
     *
     * @param integer $mo
     * @param integer $year
     * @param integer $quarter
     * @return array()
     */
    public function accountingByMo(int $mo, int $year, int $quarter = 1)
    {
        return $query = $this->createQueryBuilder('m')
            ->where("m.mo1 = :mo OR m.mo2 = :mo OR m.mo3 = :mo OR m.mo4 = :mo")->setParameter('mo', $mo)
            ->andWhere('m.year = :year')->setParameter('year', $year)
            ->getQuery()
            ->getResult();
    }

    /**
     * Выбираем записи партийцев для учета по периоду и МО с индексом по Member
     *
     * @param integer $mo
     * @param integer $year
     * @return array()
     */
    public function accountingByMoIndexByMember(int $mo, int $year, int $quarter = 1)
    {
        $result = [];

        $query = $this->createQueryBuilder('m')
            ->where("m.mo{$quarter} = :mo")->setParameter('mo', $mo)
            ->andWhere('m.year = :year')->setParameter('year', $year)
            ->getQuery()
            ->getResult();

        foreach ($query as $item) {
            $result[$item->getMember()->getId()] = [
                'paymentQuarter1' => $item->getPaymentQuarter1(),
                'paymentQuarter2' => $item->getPaymentQuarter2(),
                'paymentQuarter3' => $item->getPaymentQuarter3(),
                'paymentQuarter4' => $item->getPaymentQuarter4(),
            ];
        }

        return $result;
    }

    /**
     * Устанавливаем текущие значения МО и ПО в Учете взносов (accounting)
     *
     * @return integer
     */
    public function updateExistingsAccountings()
    {
        //  Выполняем UPDATE LEFT JOIN
        //  Выбираем всех ЧП по заданному ПО и обновляем МО, ПО и Статус по заданному Году и Кварталу
        $conn = $this->getEntityManager()
            ->getConnection();

        $sql = "UPDATE accounting as a
                LEFT JOIN member as m
                ON a.member_id = m.id
                SET
                    a.mo = m.mo_id,
                    a.po = m.po_id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        //  Возвращаем количество измененных строк
        return $stmt->rowCount();
    }
}
