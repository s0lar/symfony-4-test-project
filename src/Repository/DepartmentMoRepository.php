<?php

namespace App\Repository;

use App\Entity\DepartmentMo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DepartmentMoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DepartmentMo::class);
    }

    public function loadMO()
    {
        $return = [];

        $result = $this->createQueryBuilder('d')
            ->orderBy('d.title', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        foreach ($result as $mo) {
            $return[$mo->getId()] = $mo->getTitle();
        }

        return $return;
    }

    /**
     * Кол-во МО
     *
     * @return array
     */
    public function countMo()
    {
        $query = $this->createQueryBuilder('m');
        $query->select('COUNT(m.id) as count');

        return $query->getQuery()
            ->getOneOrNullResult();
    }
}
