<?php

namespace App\Repository;

use App\Entity\DepartmentZo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DepartmentZo|null find($id, $lockmode = null, $lockVersion = null)
 * @method DepartmentZo|null findOneBy(array $criteria, array $orderBy = null)
 * @method DepartmentZo[]    findAll()
 * @method DepartmentZo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartmentZoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DepartmentZo::class);
    }

    public function loadZO()
    {
        $return = [];

        $result = $this->createQueryBuilder('d')
            ->orderBy('d.title', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        foreach ($result as $zo) {
            $return[$zo->getId()] = $zo->getTitle();
        }

        return $return;
    }
}
