<?php

namespace App\Entity;

use App\Entity\DepartmentMo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Первичные отделения
 *
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentPoRepository")
 */
class DepartmentPo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true)
     */
    private $id;

    /**
     * moId
     * ID - Муниципального отделения
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartmentMo")
     * @ORM\JoinColumn(nullable=true)
     */
    private $moId;

    /**
     * zoId
     * ID - Округа
     *
     * @ORM\Column(nullable=false)
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartmentZo")
     */
    private $zoId;

    /**
     * title
     * Название
     *
     * @ORM\Column(type="string", length=128)
     */
    private $title;

    /**
     * titleMo
     * Название МО
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $titleMo;

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get moId
     */
    public function getMoId()
    {
        return $this->moId;
    }

    /**
     * Set moId
     *
     * @return  self
     */
    public function setMoId($moId)
    {
        $this->moId = $moId;

        return $this;
    }

    /**
     * Set roId
     *
     * @return  self
     */
    public function setRoId($roId)
    {
        $this->roId = $roId;

        return $this;
    }

    /**
     * Get title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get titleMo
     */
    public function getTitleMo()
    {
        return $this->titleMo;
    }

    /**
     * Set titleMo
     *
     * @return  self
     */
    public function setTitleMo($titleMo)
    {
        $this->titleMo = $titleMo;

        return $this;
    }

    /**
     * Get zoId
     */
    public function getZoId()
    {
        return $this->zoId;
    }

    /**
     * Set zoId
     *
     * @return  self
     */
    public function setZoId($zoId)
    {
        $this->zoId = $zoId;

        return $this;
    }

    /**
     * Magic to string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
        // return $this->getId();
    }

    /**
     * Проверка доступа пользователя к Первичному отделению
     *
     * @param User $user
     * @return boolean
     */
    public function isMoOwner(User $user = null)
    {
        //  Если пользователь Админ, то есть доступ
        if ($user && in_array('ROLE_ADMIN', $user->getRoles())) {
            return true;
        }

        //  Если пользователь - оператор, то проверяем заданный МО
        else {
            return $user &&
            in_array('ROLE_USER', $user->getRoles()) &&
            $user->getDepartmentMo()->getId() === $this->getMoId();
        }
    }
}
