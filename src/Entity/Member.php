<?php

namespace App\Entity;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\DepartmentRo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MemberRepository")
 * @ORM\Table(indexes={
 *      @ORM\Index(name="status_idx", columns={"status"}),
 *      @ORM\Index(name="last_name_idx", columns={"last_name"}),
 *      @ORM\Index(name="first_name_idx", columns={"first_name"}),
 *      @ORM\Index(name="middle_name_idx", columns={"middle_name"}),
 *      @ORM\Index(name="app_status_idx", columns={"app_status"}),
 *      @ORM\Index(name="status_idx", columns={"status"}),
 *      @ORM\Index(name="party_ticket_idx", columns={"party_ticket"}),
 *      @ORM\Index(name="birthday_idx", columns={"birthday"})
 * })
 */
class Member
{
    /**
     * Количество позиций в списке
     */
    const NUM_ITEMS = 20;

    /**
     * Статусы
     *
     * statusList
     */
    private $statusList = array(
        '1' => 'По смерти выбытие',
        '2' => 'Исключение из Партии',
        '3' => 'Член Партии',
        '4' => 'Снятие с учета',
        '5' => 'Приостановление',
        '6' => 'Сторонник Партии',
        '7' => 'Добровольный выход',
        '8' => 'По краю Переезд',
        '9' => 'За край Переезд',
        '10' => 'Уприостан-ие',
    );

    /**
     * Образование
     *
     * educationList
     */
    private $educationList = array(
        '1' => 'Неполное среднее',
        '2' => 'Среднее',
        '3' => 'Среднее специальное',
        '4' => 'Незаконченное высшее',
        '5' => 'Высшее',
    );

    /**
     * Ученая степень
     *
     * degreeList
     */
    private $degreeList = array(
        '1' => 'Кандидат наук',
        '2' => 'Доктор наук',
        '3' => 'Доцент',
        '4' => 'Профессор',
        '5' => 'Академик',
    );

    /**
     * Награды и звания
     *
     * rankList
     */
    private $rankList = array(
        '1' => 'Герой РФ',
        '2' => 'Герой СССР',
        '3' => 'Ордена',
        '4' => 'Медали',
        '5' => 'Знаки отличия',
        '6' => 'Почетные звания',
    );

    /**
     * Социальная категория
     *
     * socialList
     */
    private $socialList = array(
        '1' => 'Рабочий',
        '2' => 'Служащий',
        '3' => 'Предприниматели',
        '4' => 'Пенсионеры',
        '5' => 'Учащиеся и студенты',
        '6' => 'Временно неработающие',
        '7' => 'Прочие',
    );

    /**
     * Сфера деятельности
     *
     * workList
     */
    private $workList = array(
        '1' => 'Сельское хозяйство',
        '2' => 'Промышленность',
        '3' => 'Образование',
        '4' => 'Госслужба',
        '5' => 'Здравоохранение',
        '6' => 'Культура и искусство',
        '7' => 'Органы и власти',
        '8' => 'Наука',
        '9' => 'Прочее',
    );

    /**
     * Участие в органах. Место работы
     *
     * govermentPlaceList
     */
    private $govermentPlaceList = array(
        '1' => 'Федеральный',
        '2' => 'Региональный',
        '3' => 'Муниципальное образование',
        '4' => 'Населенный пункт в составе МО',
    );

    /**
     * Участие в органах. Уровень
     *
     * govermentLevelList
     */
    private $govermentLevelList = array(
        '1' => 'Федеральный',
        '2' => 'Региональный',
        '3' => 'Муниципальное образование',
        '4' => 'Населенный пункт в составе МО',
    );

    /**
     * Участие в органах. Депутат уровень
     *
     * govermentDeputyLevelList
     */
    private $govermentDeputyLevelList = array(
        '1' => 'Федеральный',
        '2' => 'Региональный',
        '3' => 'Муниципальный',
    );

    /**
     * Должность в партии
     *
     * partyPlaceList
     */
    private $partyPlaceList = array(
        '19' => 'член Регионального политсовета',
        '20' => 'Председатель РКРК',
        '21' => 'заместитель Председателя РКРК',
        '22' => 'член РКРК',
        '25' => 'член Совета сторонников Партии',
        '27' => 'Секретарь Местного политсовета',
        '28' => 'заместитель Секретаря Местного политсовета',
        '29' => 'член Местного политсовета',
        '30' => 'Руководитель Местного исполкома',
        '31' => 'Председатель МКРК',
        '32' => 'член МКРК',
        '33' => 'Секретарь Совета первичного отделения',
        '34' => 'член Совета первичного отделения',
        '35' => 'Секретарь первичного отделения',
        '36' => 'Ревизор первичного отделения',
    );

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * ro
     * ID - Субъект федерации
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartmentRo")
     * @ORM\JoinColumn(name="ro_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $ro;

    /**
     * mo
     * ID - Муниципальное отделение
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartmentMo")
     * @ORM\JoinColumn(name="mo_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $mo;

    /**
     * po
     * ID - Первичное отделение
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartmentPo")
     * @ORM\JoinColumn(name="po_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $po;

    /**
     * status
     * Статус
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * lastName
     * Фамилия
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $lastName;

    /**
     * firstName
     * Имя
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $firstName;

    /**
     * middleName
     * Отчество
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $middleName;

    /**
     * birthday
     * Дата рождения
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * sex
     * Пол
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sex;

    /**
     * city
     * Город
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $city;

    /**
     * street
     * Улица
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $street;

    /**
     * post
     * Индекс
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $post;

    /**
     * house
     * Дом
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $house;

    /**
     * block
     * Корпус
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $block;

    /**
     * flat
     * Квартира
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $flat;

    /**
     * passportSeries
     * Паспорт серия
     *
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $passportSeries;

    /**
     * passportNumber
     * Паспорт номер
     *
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $passportNumber;

    /**
     * passportDepartment
     * Паспорт кем выдан
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $passportDepartment;

    /**
     * passportDate
     * Паспорт - дата выдачи
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $passportDate;

    /**
     * adherentDate
     * Сторонник с
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $adherentDate;

    /**
     * requestDate
     * Дата заявления
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $requestDate;

    /**
     * phoneHome
     * Телефон - домашний
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $phoneHome;

    /**
     * phoneWork
     * Телефон - Рабочий
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $phoneWork;

    /**
     * phoneMobile
     * Телефон - сотовый
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $phoneMobile;

    /**
     * email
     * Email
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $email;

    /**
     * education
     * Образование
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $education;

    /**
     * degree
     * Ученая степень
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $degree;

    /**
     * rank
     * Звание
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rank;

    /**
     * workPlace
     * Место работы
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $workPlace;

    /**
     * workPost
     * Должность
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $workPost;

    /**
     * socialCategory
     * Социальная категория
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $socialCategory;

    /**
     * workSphere
     * Сфера деятельности
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $workSphere;

    /**
     * govermentMember
     * Участие в органах
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $govermentMember;

    /**
     * govermentDateStart
     * Участие в органах. Год начала
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $govermentDateStart;

    /**
     * govermentDateEnd
     * Участие в органах. Год окончания
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $govermentDateEnd;

    /**
     * govermentPlace
     * Участие в органах. Место работы
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $govermentPlace;

    /**
     * govermentLevel
     * Участие в органах. Уровень
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $govermentLevel;

    /**
     * govermentPost
     * Должность
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $govermentPost;

    /**
     * govermentDeputy
     * Участие в органах. Депутат?
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $govermentDeputy;

    /**
     * govermentDeputyLevel
     * Участие в органах. Депутат, уровень
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $govermentDeputyLevel;

    /**
     * govermentDeputyPlace
     * Участие в органах. Депутат, наименование органа
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $govermentDeputyPlace;

    /**
     * partyIn
     * Выборные и руководящие органы партии
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $partyIn;

    /**
     * partyPlace
     * Должность в партии
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $partyPlace;

    /**
     * protocol
     * № протокола
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $protocol;

    /**
     * acceptedBy
     * Кем принят
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $acceptedBy;

    /**
     * departmentName
     * Название отделения
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $departmentName;

    /**
     * acceptedDate
     * Дата принятия
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $acceptedDate;

    /**
     * pollingDepartment
     * № избирательного участка
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pollingDepartment;

    /**
     * partyTicket
     * № партийного билета
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $partyTicket;

    /**
     * partyTicketDate
     * Дата получения партийного билета
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $partyTicketDate;

    /**
     * partyExitDate
     * Дата выбытия
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $partyExitDate;

    /**
     * highlighted
     * Сумма всего
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $highlighted;

    /**
     * amountClaimed
     * Выделен
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amountClaimed;

    /**
     * amountPaid
     * Сумма взноса
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amountPaid;

    /**
     * amountTotal
     * Сумма всего
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amountTotal;

    /**
     * appStatus
     * Статус анкеты
     *
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $appStatus;

    /**
     * createdBy
     * Кто добавил
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $createdBy;

    /**
     * updatedBy
     * Кто последний вносил изменения
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updatedBy;

    /**
     * createdAt
     * Дата создание
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $createdAt;

    /**
     * updatedAt
     * Дата последнего изменения
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $updatedAt;

    /**
     * log
     * Лог действий над пользователем
     *
     * @ORM\Column(type="array", nullable=true)
     */
    private $log;

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get статусы
     */
    public function getStatusList()
    {
        return $this->statusList;
    }

    /**
     * Get образование
     */
    public function getEducationList()
    {
        return $this->educationList;
    }

    /**
     * Get ученая степень
     */
    public function getDegreeList()
    {
        return $this->degreeList;
    }

    /**
     * Get награды и звания
     */
    public function getRankList()
    {
        return $this->rankList;
    }

    /**
     * Get социальная категория
     */
    public function getSocialList()
    {
        return $this->socialList;
    }

    /**
     * Get сфера деятельности
     */
    public function getWorkList()
    {
        return $this->workList;
    }

    /**
     * Get участие в органах. Место работы
     */
    public function getGovermentPlaceList()
    {
        return $this->govermentPlaceList;
    }

    /**
     * Get участие в органах. Уровень
     */
    public function getGovermentLevelList()
    {
        return $this->govermentLevelList;
    }

    /**
     * Get участие в органах. Депутат уровень
     */
    public function getGovermentDeputyLevelList()
    {
        return $this->govermentDeputyLevelList;
    }

    /**
     * Get должность в партии
     */
    public function getPartyPlaceList()
    {
        return $this->partyPlaceList;
    }

    /**
     * Get status
     */
    public function getStatus($inText = false)
    {
        if ($inText) {
            // $arr = $this->getStatusList();
            return (isset($this->getStatusList()[$this->status])) ? $this->getStatusList()[$this->status] : null;
        } else {
            return $this->status;
        }
    }

    /**
     * Set status
     *
     * @return  self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set lastName
     *
     * @return  self
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @return  self
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get middleName
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set middleName
     *
     * @return  self
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get birthday
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set birthday
     *
     * @return  self
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get sex
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set sex
     *
     * @return  self
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @return  self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street
     *
     * @return  self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set post
     *
     * @return  self
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get house
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set house
     *
     * @return  self
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get block
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set block
     *
     * @return  self
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get flat
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * Set flat
     *
     * @return  self
     */
    public function setFlat($flat)
    {
        $this->flat = $flat;

        return $this;
    }

    /**
     * Get passportSeries
     */
    public function getPassportSeries()
    {
        return $this->passportSeries;
    }

    /**
     * Set passportSeries
     *
     * @return  self
     */
    public function setPassportSeries($passportSeries)
    {
        $this->passportSeries = $passportSeries;

        return $this;
    }

    /**
     * Get passportNumber
     */
    public function getPassportNumber()
    {
        return $this->passportNumber;
    }

    /**
     * Set passportNumber
     *
     * @return  self
     */
    public function setPassportNumber($passportNumber)
    {
        $this->passportNumber = $passportNumber;

        return $this;
    }

    /**
     * Get passportDepartment
     */
    public function getPassportDepartment()
    {
        return $this->passportDepartment;
    }

    /**
     * Set passportDepartment
     *
     * @return  self
     */
    public function setPassportDepartment($passportDepartment)
    {
        $this->passportDepartment = $passportDepartment;

        return $this;
    }

    /**
     * Get passportDate
     */
    public function getPassportDate()
    {
        return $this->passportDate;
    }

    /**
     * Set passportDate
     *
     * @return  self
     */
    public function setPassportDate($passportDate)
    {
        $this->passportDate = $passportDate;

        return $this;
    }

    /**
     * Get adherentDate
     */
    public function getAdherentDate()
    {
        return $this->adherentDate;
    }

    /**
     * Set adherentDate
     *
     * @return  self
     */
    public function setAdherentDate($adherentDate)
    {
        $this->adherentDate = $adherentDate;

        return $this;
    }

    /**
     * Get requestDate
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * Set requestDate
     *
     * @return  self
     */
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;

        return $this;
    }

    /**
     * Get phoneHome
     */
    public function getPhoneHome()
    {
        return $this->phoneHome;
    }

    /**
     * Set phoneHome
     *
     * @return  self
     */
    public function setPhoneHome($phoneHome)
    {
        $this->phoneHome = $phoneHome;

        return $this;
    }

    /**
     * Get phoneWork
     */
    public function getPhoneWork()
    {
        return $this->phoneWork;
    }

    /**
     * Set phoneWork
     *
     * @return  self
     */
    public function setPhoneWork($phoneWork)
    {
        $this->phoneWork = $phoneWork;

        return $this;
    }

    /**
     * Get phoneMobile
     */
    public function getPhoneMobile()
    {
        return $this->phoneMobile;
    }

    /**
     * Set phoneMobile
     *
     * @return  self
     */
    public function setPhoneMobile($phoneMobile)
    {
        $this->phoneMobile = $phoneMobile;

        return $this;
    }

    /**
     * Get email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get education
     */
    public function getEducation($inText = false)
    {
        if ($inText) {
            return (isset($this->getEducationList()[$this->education])) ? $this->getEducationList()[$this->education] : null;
        } else {
            return $this->education;
        }
    }

    /**
     * Set education
     *
     * @return  self
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get degree
     */
    public function getDegree($inText = false)
    {
        if ($inText) {
            return (isset($this->getDegreeList()[$this->degree])) ? $this->getDegreeList()[$this->degree] : null;
        } else {
            return $this->degree;
        }
    }

    /**
     * Set degree
     *
     * @return  self
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * Get rank
     */
    public function getRank($inText = false)
    {
        if ($inText) {
            return (isset($this->getRankList()[$this->rank])) ? $this->getRankList()[$this->rank] : null;
        } else {
            return $this->rank;
        }
    }

    /**
     * Set rank
     *
     * @return  self
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get workPlace
     */
    public function getWorkPlace()
    {
        return $this->workPlace;
    }

    /**
     * Set workPlace
     *
     * @return  self
     */
    public function setWorkPlace($workPlace)
    {
        $this->workPlace = $workPlace;

        return $this;
    }

    /**
     * Get workPost
     */
    public function getWorkPost()
    {
        return $this->workPost;
    }

    /**
     * Set workPost
     *
     * @return  self
     */
    public function setWorkPost($workPost)
    {
        $this->workPost = $workPost;

        return $this;
    }

    /**
     * Get socialCategory
     */
    public function getSocialCategory($inText = false)
    {
        if ($inText) {
            return (isset($this->getSocialList()[$this->socialCategory])) ? $this->getSocialList()[$this->socialCategory] : null;
        } else {
            return $this->socialCategory;
        }
    }

    /**
     * Set socialCategory
     *
     * @return  self
     */
    public function setSocialCategory($socialCategory)
    {
        $this->socialCategory = $socialCategory;

        return $this;
    }

    /**
     * Get workSphere
     */
    public function getWorkSphere($inText = false)
    {
        if ($inText) {
            return (isset($this->getWorkList()[$this->workSphere])) ? $this->getWorkList()[$this->workSphere] : null;
        } else {
            return $this->workSphere;
        }
    }

    /**
     * Set workSphere
     *
     * @return  self
     */
    public function setWorkSphere($workSphere)
    {
        $this->workSphere = $workSphere;

        return $this;
    }

    /**
     * Get inGoverment
     */
    public function getInGoverment()
    {
        return $this->inGoverment;
    }

    /**
     * Set inGoverment
     *
     * @return  self
     */
    public function setInGoverment($inGoverment)
    {
        $this->inGoverment = $inGoverment;

        return $this;
    }

    /**
     * Get govermentMember
     */
    public function getGovermentMember()
    {
        return $this->govermentMember;
    }

    /**
     * Set govermentMember
     *
     * @return  self
     */
    public function setGovermentMember($govermentMember)
    {
        $this->govermentMember = $govermentMember;

        return $this;
    }

    /**
     * Get govermentDateStart
     */
    public function getGovermentDateStart()
    {
        return $this->govermentDateStart;
    }

    /**
     * Set govermentDateStart
     *
     * @return  self
     */
    public function setGovermentDateStart($govermentDateStart)
    {
        $this->govermentDateStart = $govermentDateStart;

        return $this;
    }

    /**
     * Get govermentDateEnd
     */
    public function getGovermentDateEnd()
    {
        return $this->govermentDateEnd;
    }

    /**
     * Set govermentDateEnd
     *
     * @return  self
     */
    public function setGovermentDateEnd($govermentDateEnd)
    {
        $this->govermentDateEnd = $govermentDateEnd;

        return $this;
    }

    /**
     * Get govermentPlace
     */
    public function getGovermentPlace($inText = false)
    {
        if ($inText) {
            return (isset($this->getGovermentPlaceList()[$this->govermentPlace])) ? $this->getGovermentPlaceList()[$this->govermentPlace] : null;
        } else {
            return $this->govermentPlace;
        }
    }

    /**
     * Set govermentPlace
     *
     * @return  self
     */
    public function setGovermentPlace($govermentPlace)
    {
        $this->govermentPlace = $govermentPlace;

        return $this;
    }

    /**
     * Get govermentLevel
     */
    public function getGovermentLevel($inText = false)
    {
        if ($inText) {
            return (isset($this->getGovermentLevelList()[$this->govermentLevel])) ? $this->getGovermentLevelList()[$this->govermentLevel] : null;
        } else {
            return $this->govermentLevel;
        }
    }

    /**
     * Set govermentLevel
     *
     * @return  self
     */
    public function setGovermentLevel($govermentLevel)
    {
        $this->govermentLevel = $govermentLevel;

        return $this;
    }

    /**
     * Get govermentPost
     */
    public function getGovermentPost()
    {
        return $this->govermentPost;
    }

    /**
     * Set govermentPost
     *
     * @return  self
     */
    public function setGovermentPost($govermentPost)
    {
        $this->govermentPost = $govermentPost;

        return $this;
    }

    /**
     * Get govermentDeputy
     */
    public function getGovermentDeputy()
    {
        return $this->govermentDeputy;
    }

    /**
     * Set govermentDeputy
     *
     * @return  self
     */
    public function setGovermentDeputy($govermentDeputy)
    {
        $this->govermentDeputy = $govermentDeputy;

        return $this;
    }

    /**
     * Get govermentDeputyLevel
     */
    public function getGovermentDeputyLevel($inText = false)
    {
        if ($inText) {
            return (isset($this->getGovermentDeputyLevelList()[$this->govermentDeputyLevel])) ? $this->getGovermentDeputyLevelList()[$this->govermentDeputyLevel] : null;
        } else {
            return $this->govermentDeputyLevel;
        }
    }

    /**
     * Set govermentDeputyLevel
     *
     * @return  self
     */
    public function setGovermentDeputyLevel($govermentDeputyLevel)
    {
        $this->govermentDeputyLevel = $govermentDeputyLevel;

        return $this;
    }

    /**
     * Get govermentDeputyPlace
     */
    public function getGovermentDeputyPlace()
    {
        return $this->govermentDeputyPlace;
    }

    /**
     * Set govermentDeputyPlace
     *
     * @return  self
     */
    public function setGovermentDeputyPlace($govermentDeputyPlace)
    {
        $this->govermentDeputyPlace = $govermentDeputyPlace;

        return $this;
    }

    /**
     * Get partyIn
     */
    public function getPartyIn()
    {
        return $this->partyIn;
    }

    /**
     * Set partyIn
     *
     * @return  self
     */
    public function setPartyIn($partyIn)
    {
        $this->partyIn = $partyIn;

        return $this;
    }

    /**
     * Get partyPlace
     */
    public function getPartyPlace($inText = false)
    {
        if ($inText) {
            return (isset($this->getPartyPlaceList()[$this->partyPlace])) ? $this->getPartyPlaceList()[$this->partyPlace] : null;
        } else {
            return $this->partyPlace;
        }
    }

    /**
     * Set partyPlace
     *
     * @return  self
     */
    public function setPartyPlace($partyPlace)
    {
        $this->partyPlace = $partyPlace;

        return $this;
    }

    /**
     * Get protocol
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * Set protocol
     *
     * @return  self
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * Get acceptedBy
     */
    public function getAcceptedBy()
    {
        return $this->acceptedBy;
    }

    /**
     * Set acceptedBy
     *
     * @return  self
     */
    public function setAcceptedBy($acceptedBy)
    {
        $this->acceptedBy = $acceptedBy;

        return $this;
    }

    /**
     * Get departmentName
     */
    public function getDepartmentName()
    {
        return $this->departmentName;
    }

    /**
     * Set departmentName
     *
     * @return  self
     */
    public function setDepartmentName($departmentName)
    {
        $this->departmentName = $departmentName;

        return $this;
    }

    /**
     * Get acceptedDate
     */
    public function getAcceptedDate()
    {
        return $this->acceptedDate;
    }

    /**
     * Set acceptedDate
     *
     * @return  self
     */
    public function setAcceptedDate($acceptedDate)
    {
        $this->acceptedDate = $acceptedDate;

        return $this;
    }

    /**
     * Get pollingDepartment
     */
    public function getPollingDepartment()
    {
        return $this->pollingDepartment;
    }

    /**
     * Set pollingDepartment
     *
     * @return  self
     */
    public function setPollingDepartment($pollingDepartment)
    {
        $this->pollingDepartment = $pollingDepartment;

        return $this;
    }

    /**
     * Get partyTicket
     */
    public function getPartyTicket()
    {
        return $this->partyTicket;
    }

    /**
     * Set partyTicket
     *
     * @return  self
     */
    public function setPartyTicket($partyTicket)
    {
        $this->partyTicket = $partyTicket;

        return $this;
    }

    /**
     * Get partyTicketDate
     */
    public function getPartyTicketDate()
    {
        return $this->partyTicketDate;
    }

    /**
     * Set partyTicketDate
     *
     * @return  self
     */
    public function setPartyTicketDate($partyTicketDate)
    {
        $this->partyTicketDate = $partyTicketDate;

        return $this;
    }

    /**
     * Get partyExitDate
     */
    public function getPartyExitDate()
    {
        return $this->partyExitDate;
    }

    /**
     * Set partyExitDate
     *
     * @return  self
     */
    public function setPartyExitDate($partyExitDate)
    {
        $this->partyExitDate = $partyExitDate;

        return $this;
    }

    /**
     * Get highlighted
     */
    public function getHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * Set highlighted
     *
     * @return  self
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;

        return $this;
    }

    /**
     * Get amountClaimed
     */
    public function getAmountClaimed()
    {
        return $this->amountClaimed;
    }

    /**
     * Set amountClaimed
     *
     * @return  self
     */
    public function setAmountClaimed($amountClaimed)
    {
        $this->amountClaimed = $amountClaimed;

        return $this;
    }

    /**
     * Get amountPaid
     */
    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    /**
     * Set amountPaid
     *
     * @return  self
     */
    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    /**
     * Get amountTotal
     */
    public function getAmountTotal()
    {
        return $this->amountTotal;
    }

    /**
     * Set amountTotal
     *
     * @return  self
     */
    public function setAmountTotal($amountTotal)
    {
        $this->amountTotal = $amountTotal;

        return $this;
    }

    /**
     * Get appStatus
     */
    public function getAppStatus()
    {
        return $this->appStatus;
    }

    /**
     * Set appStatus
     *
     * @return  self
     */
    public function setAppStatus($appStatus)
    {
        $this->appStatus = $appStatus;

        return $this;
    }

    /**
     * Get createdBy
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @return  self
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy
     *
     * @return  self
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @return  self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @return  self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get ro
     */
    public function getRo()
    {
        return $this->ro;
    }

    /**
     * Set roId
     *
     * @return  self
     */
    public function setRo($ro)
    {
        $this->ro = $ro;

        return $this;
    }

    /**
     * Get moId
     */
    public function getMo()
    {
        return $this->mo;
    }

    /**
     * Set moId
     *
     * @return  self
     */
    public function setMo($mo)
    {
        $this->mo = $mo;

        return $this;
    }

    /**
     * Get po
     */
    public function getPo()
    {
        return $this->po;
    }

    /**
     * Set po
     *
     * @return  self
     */
    public function setPo($po)
    {
        $this->po = $po;

        return $this;
    }

    /**
     * Get log
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Set log
     *
     * @return  self
     */
    public function setLog($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Add log
     *
     * @return  self
     */
    public function addLog($log)
    {
        $this->log[] = $log;
        // dump($this->log);
        // dump($this->getLog());
        // exit;

        return $this;
    }

    /**
     * Get text status
     */
    public function getTextStatus($status)
    {
        return (isset($this->getStatusList()[$status])) ? $this->getStatusList()[$status] : null;
    }

    public function __toString()
    {
        return $this->getLastName();
    }

    public function isMoOwner(User $user = null)
    {
        //  Если пользователь Админ, то есть доступ
        if ($user && in_array('ROLE_ADMIN', $user->getRoles())) {
            return true;
        }

        //  Если пользователь - оператор, то проверяем заданный МО
        else {
            return $user &&
            in_array('ROLE_USER', $user->getRoles()) &&
            $user->getDepartmentMo() === $this->getMo();
        }
    }
}
