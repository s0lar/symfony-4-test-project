<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Субъекты федерации
 *
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentRoRepository")
 */
class DepartmentRo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", unique=true)
     */
    private $id;

    /**
     * title
     * Название
     *
     * @ORM\Column(type="string", length=128)
     */
    private $title;

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    /**
     * Get title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }
}
