<?php
// src/Entity/User.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use App\Entity\DepartmentMo;

/**
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @ORM\Column(name="is_non_locked", type="boolean")
     */
    private $isNonLocked;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="reset_url", type="string", length=64, nullable=true)
     */
    private $resetUrl;

    /**
     * @ORM\Column(name="reset_expire_at", type="datetime", length=64, nullable=true)
     */
    private $resetExpireAt;

    /**
     * @ORM\Column(name="activation_url", type="string", length=64, nullable=true)
     */
    private $activationUrl;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartmentMo")
     * @ORM\JoinColumn(nullable=true)
     */
    private $departmentMo;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    //  Roles list
    private $rolesList = array(
        'ROLE_ADMIN' => 'Оператор РИК',
        'ROLE_USER' => 'Оператор МИК',
        // 'ROLE_REPORT' => 'Бухгалтер',
    );

    public function __construct()
    {
        $this->isActive = false;
        $this->isNonLocked = true;
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }


    /**
     * Get the value of roles
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set the value of roles
     *
     * @return  self
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return $this->isNonLocked;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isNonLocked,
            $this->isActive,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password,
            $this->isNonLocked,
            $this->isActive,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set the value of isActive
     *
     * @return  self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get the value of isNonLocked
     */
    public function getIsNonLocked()
    {
        return $this->isNonLocked;
    }

    /**
     * Set the value of isNonLocked
     *
     * @return  self
     */
    public function setIsNonLocked($isNonLocked)
    {
        $this->isNonLocked = $isNonLocked;

        return $this;
    }

    /**
     * Get the value of resetUrl
     */
    public function getResetUrl()
    {
        return $this->resetUrl;
    }

    /**
     * Set the value of resetUrl
     *
     * @return  self
     */
    public function setResetUrl($resetUrl)
    {
        $this->resetUrl = $resetUrl;

        return $this;
    }

    /**
     * Get the value of resetExpireAt
     */
    public function getResetExpireAt()
    {
        return $this->resetExpireAt;
    }

    /**
     * Set the value of resetExpireAt
     *
     * @return  self
     */
    public function setResetExpireAt($resetExpireAt)
    {
        $this->resetExpireAt = $resetExpireAt;

        return $this;
    }

    /**
     * Get the value of activationUrl
     */
    public function getActivationUrl()
    {
        return $this->activationUrl;
    }

    /**
     * Set the value of activationUrl
     *
     * @return  self
     */
    public function setActivationUrl($activationUrl)
    {
        $this->activationUrl = $activationUrl;

        return $this;
    }

    /**
     * Get the value of createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @return  self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of rolesList
     */
    public function getRolesList()
    {
        return $this->rolesList;
    }

    /**
     * Get the value of departmentMo
     */
    public function getDepartmentMo()
    {
        return $this->departmentMo;
    }

    /**
     * Set the value of departmentMo
     *
     * @return  self
     */
    public function setDepartmentMo(DepartmentMo $departmentMo = null)
    {
        $this->departmentMo = $departmentMo;

        return $this;
    }
}