<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\DepartmentRo;

/**
 * Муниципальные отделения
 *
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentMoRepository")
 * @ORM\Table(indexes={
 *      @ORM\Index(name="title_idx", columns={"title"}),
 * })
 */
class DepartmentMo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true)
     */
    private $id;

    /**
     * roId
     * ID - Субъекта федерации
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartmentRo")
     * @ORM\JoinColumn(nullable=true)
     */
    private $roId;

    /**
     * title
     * Название
     *
     * @ORM\Column(type="string", length=128)
     */
    private $title;

    /**
     * titleRegion
     * Название района
     *
     * @ORM\Column(type="string", length=128)
     */
    private $titleRegion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DepartmentZo", mappedBy="mo")
     */
    private $zoId;

    public function __construct()
    {
        $this->zoId = new ArrayCollection();
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get roId
     */
    public function getRoId()
    {
        return $this->roId;
    }

    /**
     * Set roId
     *
     * @return  self
     */
    public function setRoId($roId)
    {
        $this->roId = $roId;

        return $this;
    }

    /**
     * Get title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Проверка доступа пользователя к Муниципальному отделению
     *
     * @param User $user
     * @return boolean
     */
    public function isMoOwner(User $user = null)
    {
        //  Если пользователь Админ, то есть доступ
        if ($user && in_array('ROLE_ADMIN', $user->getRoles())) {
            return true;
        }

        //  Если пользователь - оператор, то проверяем заданный МО
        else {
            return $user &&
                in_array('ROLE_USER', $user->getRoles()) &&
                $this === $user->getDepartmentMo();
        }
    }

    /**
     * Get titleRegion
     */
    public function getTitleRegion()
    {
        return $this->titleRegion;
    }

    /**
     * Set titleRegion
     *
     * @return  self
     */
    public function setTitleRegion($titleRegion)
    {
        $this->titleRegion = $titleRegion;

        return $this;
    }

    /**
     * @return Collection|DepartmentZo[]
     */
    public function getZoId(): Collection
    {
        return $this->zoId;
    }

    public function addZoId(DepartmentZo $zoId): self
    {
        if (!$this->zoId->contains($zoId)) {
            $this->zoId[] = $zoId;
            $zoId->setMo($this);
        }

        return $this;
    }

    public function removeZoId(DepartmentZo $zoId): self
    {
        if ($this->zoId->contains($zoId)) {
            $this->zoId->removeElement($zoId);
            // set the owning side to null (unless already changed)
            if ($zoId->getMo() === $this) {
                $zoId->setMo(null);
            }
        }

        return $this;
    }
}
