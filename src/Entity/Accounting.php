<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountingRepository")
 * @ORM\Table(
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="member_year_idx", columns={"member_id", "year"})
 *      },
 *      indexes={
 *          @ORM\Index(name="mo_idx", columns={"mo"}),
 *          @ORM\Index(name="po_idx", columns={"po"}),
 *          @ORM\Index(name="year_idx", columns={"year"})
 *      })
 */
class Accounting
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * member
     * Член партии
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Member")
     * @ORM\JoinColumn(nullable=true, name="member_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    private $member;

    /**
     * mo
     * Местное отделение
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mo;

    /**
     * po
     * Первичное отделение
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $po;

    /**
     * mo1
     * Местное отделение в 1 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $mo1;

    /**
     * po1
     * Первичное отделение в 1 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $po1;

    /**
     * mo2
     * Местное отделение в 2 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $mo2;

    /**
     * po2
     * Первичное отделение в 2 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $po2;

    /**
     * mo3
     * Местное отделение в 3 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $mo3;

    /**
     * po3
     * Первичное отделение в 3 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $po3;

    /**
     * mo4
     * Местное отделение в 4 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $mo4;

    /**
     * po4
     * Первичное отделение в 4 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $po4;

    /**
     * status1
     * Статус ЧП в 1 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $status1;

    /**
     * status2
     * Статус ЧП в 2 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $status2;

    /**
     * status3
     * Статус ЧП в 3 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $status3;

    /**
     * status4
     * Статус ЧП в 4 квартале
     *
     * @ORM\Column(type="integer", nullable=true))
     */
    private $status4;

    /**
     * year
     * Отчетный год
     *
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * paymentDeclared1
     * Заявленная сумма взнос за 1 квартал
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentDeclared1;

    /**
     * paymentDeclared2
     * Заявленная сумма взнос за 2 квартал
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentDeclared2;

    /**
     * paymentDeclared3
     * Заявленная сумма взнос за 3 квартал
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentDeclared3;

    /**
     * paymentDeclared4
     * Заявленная сумма взнос за 4 квартал
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentDeclared4;

    /**
     * paymentQuarter1
     * Взнос за 1 квартал
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentQuarter1;

    /**
     * paymentQuarter2
     * Взнос за 2 квартал
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentQuarter2;

    /**
     * paymentQuarter3
     * Взнос за 3 квартал
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentQuarter3;

    /**
     * paymentQuarter4
     * Взнос за 4 квартал
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentQuarter4;

    /**
     * dateQuarter1
     * Дата взноса за 1 квартал
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateQuarter1;

    /**
     * dateQuarter2
     * Дата взноса за 2 квартал
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateQuarter2;

    /**
     * dateQuarter3
     * Дата взноса за 3 квартал
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateQuarter3;

    /**
     * dateQuarter4
     * Дата взноса за 4 квартал
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateQuarter4;

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set member
     *
     * @return  self
     */
    public function setMember($member)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get mo
     */
    public function getMo()
    {
        return $this->mo;
    }

    /**
     * Set mo
     *
     * @return  self
     */
    public function setMo($mo)
    {
        $this->mo = $mo;

        return $this;
    }

    /**
     * Get po
     */
    public function getPo()
    {
        return $this->po;
    }

    /**
     * Set po
     *
     * @return  self
     */
    public function setPo($po)
    {
        $this->po = $po;

        return $this;
    }

    /**
     * Get year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set year
     *
     * @return  self
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get paymentQuarter1
     */
    public function getPaymentQuarter1()
    {
        return $this->paymentQuarter1;
    }

    /**
     * Set paymentQuarter1
     *
     * @return  self
     */
    public function setPaymentQuarter1($paymentQuarter1)
    {
        $this->paymentQuarter1 = $paymentQuarter1;

        return $this;
    }

    /**
     * Get paymentQuarter2
     */
    public function getPaymentQuarter2()
    {
        return $this->paymentQuarter2;
    }

    /**
     * Set paymentQuarter2
     *
     * @return  self
     */
    public function setPaymentQuarter2($paymentQuarter2)
    {
        $this->paymentQuarter2 = $paymentQuarter2;

        return $this;
    }

    /**
     * Get paymentQuarter3
     */
    public function getPaymentQuarter3()
    {
        return $this->paymentQuarter3;
    }

    /**
     * Set paymentQuarter3
     *
     * @return  self
     */
    public function setPaymentQuarter3($paymentQuarter3)
    {
        $this->paymentQuarter3 = $paymentQuarter3;

        return $this;
    }

    /**
     * Get paymentQuarter4
     */
    public function getPaymentQuarter4()
    {
        return $this->paymentQuarter4;
    }

    /**
     * Set paymentQuarter4
     *
     * @return  self
     */
    public function setPaymentQuarter4($paymentQuarter4)
    {
        $this->paymentQuarter4 = $paymentQuarter4;

        return $this;
    }

    /**
     * Get dateQuarter1
     */
    public function getDateQuarter1()
    {
        return $this->dateQuarter1;
    }

    /**
     * Set dateQuarter1
     *
     * @return  self
     */
    public function setDateQuarter1($dateQuarter1)
    {
        $this->dateQuarter1 = $dateQuarter1;

        return $this;
    }

    /**
     * Get dateQuarter2
     */
    public function getDateQuarter2()
    {
        return $this->dateQuarter2;
    }

    /**
     * Set dateQuarter2
     *
     * @return  self
     */
    public function setDateQuarter2($dateQuarter2)
    {
        $this->dateQuarter2 = $dateQuarter2;

        return $this;
    }

    /**
     * Get dateQuarter3
     */
    public function getDateQuarter3()
    {
        return $this->dateQuarter3;
    }

    /**
     * Set dateQuarter3
     *
     * @return  self
     */
    public function setDateQuarter3($dateQuarter3)
    {
        $this->dateQuarter3 = $dateQuarter3;

        return $this;
    }

    /**
     * Get dateQuarter4
     */
    public function getDateQuarter4()
    {
        return $this->dateQuarter4;
    }

    /**
     * Set dateQuarter4
     *
     * @return  self
     */
    public function setDateQuarter4($dateQuarter4)
    {
        $this->dateQuarter4 = $dateQuarter4;

        return $this;
    }

    /**
     * Get paymentDeclared1
     */
    public function getPaymentDeclared1()
    {
        return $this->paymentDeclared1;
    }

    /**
     * Set paymentDeclared1
     *
     * @return  self
     */
    public function setPaymentDeclared1($paymentDeclared1)
    {
        $this->paymentDeclared1 = $paymentDeclared1;

        return $this;
    }

    /**
     * Get paymentDeclared2
     */
    public function getPaymentDeclared2()
    {
        return $this->paymentDeclared2;
    }

    /**
     * Set paymentDeclared2
     *
     * @return  self
     */
    public function setPaymentDeclared2($paymentDeclared2)
    {
        $this->paymentDeclared2 = $paymentDeclared2;

        return $this;
    }

    /**
     * Get paymentDeclared3
     */
    public function getPaymentDeclared3()
    {
        return $this->paymentDeclared3;
    }

    /**
     * Set paymentDeclared3
     *
     * @return  self
     */
    public function setPaymentDeclared3($paymentDeclared3)
    {
        $this->paymentDeclared3 = $paymentDeclared3;

        return $this;
    }

    /**
     * Get paymentDeclared4
     */
    public function getPaymentDeclared4()
    {
        return $this->paymentDeclared4;
    }

    /**
     * Set paymentDeclared4
     *
     * @return  self
     */
    public function setPaymentDeclared4($paymentDeclared4)
    {
        $this->paymentDeclared4 = $paymentDeclared4;

        return $this;
    }

    /**
     * Get mo1
     */
    public function getMo1()
    {
        return $this->mo1;
    }

    /**
     * Set mo1
     *
     * @return  self
     */
    public function setMo1($mo1)
    {
        $this->mo1 = $mo1;

        return $this;
    }

    /**
     * Get po1
     */
    public function getPo1()
    {
        return $this->po1;
    }

    /**
     * Set po1
     *
     * @return  self
     */
    public function setPo1($po1)
    {
        $this->po1 = $po1;

        return $this;
    }

    /**
     * Get mo2
     */
    public function getMo2()
    {
        return $this->mo2;
    }

    /**
     * Set mo2
     *
     * @return  self
     */
    public function setMo2($mo2)
    {
        $this->mo2 = $mo2;

        return $this;
    }

    /**
     * Get po2
     */
    public function getPo2()
    {
        return $this->po2;
    }

    /**
     * Set po2
     *
     * @return  self
     */
    public function setPo2($po2)
    {
        $this->po2 = $po2;

        return $this;
    }

    /**
     * Get mo3
     */
    public function getMo3()
    {
        return $this->mo3;
    }

    /**
     * Set mo3
     *
     * @return  self
     */
    public function setMo3($mo3)
    {
        $this->mo3 = $mo3;

        return $this;
    }

    /**
     * Get po3
     */
    public function getPo3()
    {
        return $this->po3;
    }

    /**
     * Set po3
     *
     * @return  self
     */
    public function setPo3($po3)
    {
        $this->po3 = $po3;

        return $this;
    }

    /**
     * Get mo4
     */
    public function getMo4()
    {
        return $this->mo4;
    }

    /**
     * Set mo4
     *
     * @return  self
     */
    public function setMo4($mo4)
    {
        $this->mo4 = $mo4;

        return $this;
    }

    /**
     * Get po4
     */
    public function getPo4()
    {
        return $this->po4;
    }

    /**
     * Set po4
     *
     * @return  self
     */
    public function setPo4($po4)
    {
        $this->po4 = $po4;

        return $this;
    }

    /**
     * Get status1
     */
    public function getStatus1()
    {
        return $this->status1;
    }

    /**
     * Set status1
     *
     * @return  self
     */
    public function setStatus1($status1)
    {
        $this->status1 = $status1;

        return $this;
    }

    /**
     * Get status2
     */
    public function getStatus2()
    {
        return $this->status2;
    }

    /**
     * Set status2
     *
     * @return  self
     */
    public function setStatus2($status2)
    {
        $this->status2 = $status2;

        return $this;
    }

    /**
     * Get status3
     */
    public function getStatus3()
    {
        return $this->status3;
    }

    /**
     * Set status3
     *
     * @return  self
     */
    public function setStatus3($status3)
    {
        $this->status3 = $status3;

        return $this;
    }

    /**
     * Get status4
     */
    public function getStatus4()
    {
        return $this->status4;
    }

    /**
     * Set status4
     *
     * @return  self
     */
    public function setStatus4($status4)
    {
        $this->status4 = $status4;

        return $this;
    }

    /**
     * Считаем дебет/кредит по уплате за квартал. Если суммы не проставлены возвращаем NULL
     * Calculating debet for quarter
     *
     * @param [type] $quater
     * @return void
     */
    public function calculateDebit($quater)
    {
        $payed = 'paymentQuarter' . $quater;
        $declared = 'paymentDeclared' . $quater;

        if ($this->$payed === null && $this->$declared === null) {
            return null;
        } else {
            return ($this->$payed - $this->$declared);
        }
    }
}
