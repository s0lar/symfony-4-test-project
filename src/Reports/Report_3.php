<?php

namespace App\Reports;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\Member;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 3
 * Отчет №3
 * Реестр местных отделений Партии "ЕДИНАЯ РОССИЯ"
 */
class Report_3 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {
        //  Период
        $data['period'] = "По состоянию на "
        . Utility::prepareDay($data['date'])
            . " г.";

        //  Все местные отделения
        $data['mo'] = $this->em->getRepository(DepartmentMo::class)->findAll();

        //  Количество Первичных отделений по МО
        $data['po'] = $this->em->getRepository(DepartmentPo::class)->poCountByMo();

        //  Количество всех Членов партии за выбранную дату
        $data['members_total'] = $this->em->getRepository(Member::class)->membersInCountByMo($data['date']);

        //  Количество принятых Членов партии за выбранную дату
        $data['members_accepted'] = $this->em->getRepository(Member::class)->membersInCountByMo($data['date'], true);

        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {
        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        //
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_3.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();

        //  Шапка отчета
        $worksheet->setCellValue('A1', $data['period']);

        //  Копируем стиль ячейки
        $style = $worksheet->getStyleByColumnAndRow(1, 6);

        //  Ввыводим членов партии
        $row = 5;

        //  Итого по всем метрикам
        $total_members = 0;
        $total_members_accepted = 0;
        $total_members_waiting = 0;
        $total_po = 0;

        foreach ($data['mo'] as $mo) {
            $row++;
            $id = $mo->getId();

            $worksheet->setCellValue('A' . $row, $mo->getId());
            $worksheet->setCellValue('B' . $row, "Краснодарский край");
            $worksheet->setCellValue('C' . $row, $mo->getTitleRegion());
            $worksheet->setCellValue('D' . $row, $mo->getTitle());

            if (isset($data['members_total'][$id])) {
                $worksheet->setCellValue('E' . $row, $data['members_total'][$id]);
                $total_members += $data['members_total'][$id];
            }

            if (isset($data['members_accepted'][$id])) {
                $worksheet->setCellValue('F' . $row, $data['members_accepted'][$id]);
                $total_members_accepted += $data['members_accepted'][$id];
            }

            if (isset($data['members_accepted'][$id]) && isset($data['members_accepted'][$id])) {
                $worksheet->setCellValue('G' . $row, $data['members_total'][$id] - $data['members_accepted'][$id]);
                $total_members_waiting += $data['members_total'][$id] - $data['members_accepted'][$id];
            }

            if (isset($data['po'][$id])) {
                $worksheet->setCellValue('I' . $row, $data['po'][$id]);
                $total_po += $data['po'][$id];
            }

            //
            $worksheet->setCellValue('H' . $row, 0);

            //  Стилизуем
            $worksheet->duplicateStyle($style, "A{$row}:I{$row}");
        }

        //  Итого
        $worksheet->setCellValue('C4', count($data['mo']));
        $worksheet->setCellValue('E4', $total_members);
        $worksheet->setCellValue('F4', $total_members_accepted);
        $worksheet->setCellValue('G4', $total_members_waiting);
        $worksheet->setCellValue('H4', 0);
        $worksheet->setCellValue('I4', $total_po);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
