<?php

namespace App\Reports;

use App\Entity\Accounting;
use App\Entity\Member;
use App\Reports\ReportInterface;
use App\Reports\Utility;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 2
 * Отчет №2
 * Отчет об уплате взносов за квартал по Муниципальным отделениям
 */
class Report_2 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {
        $utility = new Utility();

        $data = array_merge($data, [
            //  Кол-во уплативших по кварталам
            'payed_count_1' => 0,
            'payed_count_2' => 0,
            'payed_count_3' => 0,
            'payed_count_4' => 0,
            //  Кол-во неплательщиков
            'unpayed_count_1' => 0,
            'unpayed_count_2' => 0,
            'unpayed_count_3' => 0,
            'unpayed_count_4' => 0,
            //  Сумма взноса за квартал
            'payed_sum_1' => 0,
            'payed_sum_2' => 0,
            'payed_sum_3' => 0,
            'payed_sum_4' => 0,
            //  Сумма взноса за квартал
            'prepayed_sum_1' => 0,
            'prepayed_sum_2' => 0,
            'prepayed_sum_3' => 0,
            'prepayed_sum_4' => 0,
            //  Сумма неуплат
            'unpayed_sum_1' => 0,
            'unpayed_sum_2' => 0,
            'unpayed_sum_3' => 0,
            'unpayed_sum_4' => 0,
            //  ЧП неплательщики
            'unpayed_members' => [],
        ]);

        //  Считаем количество партийцев за по кварталам за год
        //  todo: переделать на информацию из accounts
        for ($i = 1; $i <= $data['quarter']; $i++) {
            $data['members_count_' . $i] = (int) $this->em->getRepository(Member::class)->membersCount($data['mo']->getId(), $data['year'], $i);
        }

        $accounting = $this->em
            ->getRepository(Accounting::class)->accountingByMo($data['mo']->getId(), $data['year'], $data['quarter']);

        foreach ($accounting as $item) {
            $mo = $data['mo']->getId();

            //  Считаем дебет/кредит за квартал
            $tmp['debet1'] = $item->calculateDebit(1);
            $tmp['debet2'] = $item->calculateDebit(2);
            $tmp['debet3'] = $item->calculateDebit(3);
            $tmp['debet4'] = $item->calculateDebit(4);

            //  Считаем дебет/кредит по году
            $tmp['total1'] = $item->getPaymentQuarter1() - $item->getPaymentDeclared1();
            $tmp['total2'] = $item->getPaymentQuarter2() - $item->getPaymentDeclared2() + $tmp['total1'];
            $tmp['total3'] = $item->getPaymentQuarter3() - $item->getPaymentDeclared3() + $tmp['total2'];
            $tmp['total4'] = $item->getPaymentQuarter4() - $item->getPaymentDeclared4() + $tmp['total3'];

            //  Кол-во уплативших по кварталам
            if ($utility::isPayedInQuarter($item, 1) && Utility::isInMoThisQuarter($item, $mo, 1)) {
                $data['payed_count_1']++;
            }

            if ($utility::isPayedInQuarter($item, 2) && Utility::isInMoThisQuarter($item, $mo, 2)) {
                $data['payed_count_2']++;
            }

            if ($utility::isPayedInQuarter($item, 3) && Utility::isInMoThisQuarter($item, $mo, 3)) {
                $data['payed_count_3']++;
            }

            if ($utility::isPayedInQuarter($item, 4) && Utility::isInMoThisQuarter($item, $mo, 4)) {
                $data['payed_count_4']++;
            }

            //  Сумма взноса за квартал
            if (Utility::isInMoThisQuarter($item, $mo, 1)) {
                $data['payed_sum_1'] += $item->getPaymentQuarter1();
            }

            if (Utility::isInMoThisQuarter($item, $mo, 2)) {
                $data['payed_sum_2'] += $item->getPaymentQuarter2();
            }

            if (Utility::isInMoThisQuarter($item, $mo, 3)) {
                $data['payed_sum_3'] += $item->getPaymentQuarter3();
            }

            if (Utility::isInMoThisQuarter($item, $mo, 4)) {
                $data['payed_sum_4'] += $item->getPaymentQuarter4();
            }

            //  Сумма авансов за квартал
            if ($tmp['debet1'] > 0 && Utility::isInMoThisQuarter($item, $mo, 1)) {
                $data['prepayed_sum_1'] += $tmp['debet1'];
            }

            if ($tmp['debet2'] > 0 && Utility::isInMoThisQuarter($item, $mo, 2)) {
                $data['prepayed_sum_2'] += $tmp['debet2'];
            }

            if ($tmp['debet3'] > 0 && Utility::isInMoThisQuarter($item, $mo, 3)) {
                $data['prepayed_sum_3'] += $tmp['debet3'];
            }

            if ($tmp['debet4'] > 0 && Utility::isInMoThisQuarter($item, $mo, 4)) {
                $data['prepayed_sum_4'] += $tmp['debet4'];
            }

            //  Сумма неуплат за квартал
            if ($tmp['total1'] < 0 && Utility::isInMoThisQuarter($item, $mo, 1)) {
                $data['unpayed_sum_1'] += $tmp['total1'];
            }

            if ($tmp['total2'] < 0 && Utility::isInMoThisQuarter($item, $mo, 2)) {
                $data['unpayed_sum_2'] += $tmp['total2'];
            }

            if ($tmp['total3'] < 0 && Utility::isInMoThisQuarter($item, $mo, 3)) {
                $data['unpayed_sum_3'] += $tmp['total3'];
            }

            if ($tmp['total4'] < 0 && Utility::isInMoThisQuarter($item, $mo, 4)) {
                $data['unpayed_sum_4'] += $tmp['total4'];
            }

            //  Кол-во неплательщиков
            if ($utility::isUnPayedInQuarter($item, 1) && Utility::isInMoThisQuarterOrNull($item, $mo, 1)) {
                $data['unpayed_count_1']++;

                //  Сохраняем неплательщика в квартале
                if ($data['quarter'] == 1) {
                    $data['unpayed_members'][$item->getMember()->getId()] = $item;
                }
            }

            if ($utility::isUnPayedInQuarter($item, 2) && Utility::isInMoThisQuarterOrNull($item, $mo, 2)) {
                $data['unpayed_count_2']++;

                //  Сохраняем неплательщика в квартале
                if ($data['quarter'] == 2) {
                    $data['unpayed_members'][$item->getMember()->getId()] = $item;
                }
            }

            if ($utility::isUnPayedInQuarter($item, 3) && Utility::isInMoThisQuarterOrNull($item, $mo, 3)) {
                $data['unpayed_count_3']++;

                //  Сохраняем неплательщика в квартале
                if ($data['quarter'] == 3) {
                    $data['unpayed_members'][$item->getMember()->getId()] = $item;
                }
            }

            if ($utility::isUnPayedInQuarter($item, 4) && Utility::isInMoThisQuarterOrNull($item, $mo, 4)) {
                $data['unpayed_count_4']++;

                //  Сохраняем неплательщика в квартале
                if ($data['quarter'] == 4) {
                    $data['unpayed_members'][$item->getMember()->getId()] = $item;
                }
            }
        }

        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {
        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        //
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_2.xlsx');
        // $worksheet = $spreadsheet->getActiveSheet();
        $worksheet = $spreadsheet->setActiveSheetIndex(0);

        //  Шапка отчета
        $worksheet->setCellValue('A1', $data['period']);
        $worksheet->setCellValue('D2', $data['mo_title']);
        $worksheet->setCellValue('D3', $data['ro_title']);

        //  Кол-во членов партии
        $worksheet->setCellValue('B6', isset($data['members_count_1']) ? $data['members_count_1'] : '');
        $worksheet->setCellValue('B7', isset($data['members_count_2']) ? $data['members_count_2'] : '');
        $worksheet->setCellValue('B8', isset($data['members_count_3']) ? $data['members_count_3'] : '');
        $worksheet->setCellValue('B9', isset($data['members_count_4']) ? $data['members_count_4'] : '');
        $worksheet->setCellValue('B10', $data["members_count_" . $data['quarter']]);

        //  Кол-во членов уплативших взнос
        $worksheet->setCellValue('C6', ($data['quarter'] > 0) ? $data['payed_count_1'] : '');
        $worksheet->setCellValue('C7', ($data['quarter'] > 1) ? $data['payed_count_2'] : '');
        $worksheet->setCellValue('C8', ($data['quarter'] > 2) ? $data['payed_count_3'] : '');
        $worksheet->setCellValue('C9', ($data['quarter'] > 3) ? $data['payed_count_4'] : '');
        $worksheet->setCellValue('C10', $data['payed_count_' . $data['quarter']]);

        //  Сумма авансов за квартал
        $worksheet->setCellValue('E6', ($data['quarter'] > 0) ? $data['prepayed_sum_1'] : '');
        $worksheet->setCellValue('E7', ($data['quarter'] > 1) ? $data['prepayed_sum_2'] : '');
        $worksheet->setCellValue('E8', ($data['quarter'] > 2) ? $data['prepayed_sum_3'] : '');
        $worksheet->setCellValue('E9', ($data['quarter'] > 3) ? $data['prepayed_sum_4'] : '');

        //  Кол-во уплативших по кварталам
        $worksheet->setCellValue('F6', ($data['quarter'] > 0) ? $data['payed_sum_1'] : '');
        $worksheet->setCellValue('F7', ($data['quarter'] > 1) ? $data['payed_sum_2'] : '');
        $worksheet->setCellValue('F8', ($data['quarter'] > 2) ? $data['payed_sum_3'] : '');
        $worksheet->setCellValue('F9', ($data['quarter'] > 3) ? $data['payed_sum_4'] : '');

        //  Сумма неуплат за квартал
        $worksheet->setCellValue('G6', ($data['quarter'] > 0) ? $data['unpayed_sum_1'] : '');
        $worksheet->setCellValue('G7', ($data['quarter'] > 1) ? $data['unpayed_sum_2'] : '');
        $worksheet->setCellValue('G8', ($data['quarter'] > 2) ? $data['unpayed_sum_3'] : '');
        $worksheet->setCellValue('G9', ($data['quarter'] > 3) ? $data['unpayed_sum_4'] : '');
        $worksheet->setCellValue('G10', $data['unpayed_sum_' . $data['quarter']]);

        //  Количество неплательщиков
        $worksheet->setCellValue('H6', ($data['quarter'] > 0) ? $data['unpayed_count_1'] : '');
        $worksheet->setCellValue('H7', ($data['quarter'] > 1) ? $data['unpayed_count_2'] : '');
        $worksheet->setCellValue('H8', ($data['quarter'] > 2) ? $data['unpayed_count_3'] : '');
        $worksheet->setCellValue('H9', ($data['quarter'] > 3) ? $data['unpayed_count_4'] : '');
        $worksheet->setCellValue('H10', $data['unpayed_count_' . $data['quarter']]);

        ///////////////////////////
        //  Выводим неплательщиков
        if (isset($data['unpayed_members']) && !empty($data['unpayed_members'])) {
            //  Пишем во вторую вкладку
            $worksheet = $spreadsheet->setActiveSheetIndex(1);

            //  Копируем стиль ячейки
            $style = $worksheet->getStyleByColumnAndRow(1, 3);

            $i = 3;
            foreach ($data['unpayed_members'] as $accounting) {
                $i++;

                $worksheet->setCellValue('A' . $i, $accounting->getMember()->getId());
                $worksheet->setCellValue('B' . $i, $accounting->getMember()->getLastName());
                $worksheet->setCellValue('C' . $i, $accounting->getMember()->getFirstName());
                $worksheet->setCellValue('D' . $i, $accounting->getMember()->getMiddleName());
                $worksheet->setCellValue('E' . $i, $accounting->getMember()->getPartyTicket());
                $worksheet->setCellValue('F' . $i, $accounting->getPaymentDeclared1());
                $worksheet->setCellValue('G' . $i, $accounting->getPaymentDeclared2());
                $worksheet->setCellValue('H' . $i, $accounting->getPaymentDeclared3());
                $worksheet->setCellValue('I' . $i, $accounting->getPaymentDeclared4());
                $worksheet->setCellValue('J' . $i, $accounting->getPaymentQuarter1());
                $worksheet->setCellValue('K' . $i, $accounting->getPaymentQuarter2());
                $worksheet->setCellValue('L' . $i, $accounting->getPaymentQuarter3());
                $worksheet->setCellValue('M' . $i, $accounting->getPaymentQuarter4());
                $worksheet->setCellValue('N' . $i, $accounting->getPo1());
                $worksheet->setCellValue('O' . $i, $accounting->getPo2());
                $worksheet->setCellValue('P' . $i, $accounting->getPo3());
                $worksheet->setCellValue('Q' . $i, $accounting->getPo4());
                $worksheet->setCellValue('R' . $i, $accounting->getMember()->getTextStatus($accounting->getStatus1()));
                $worksheet->setCellValue('S' . $i, $accounting->getMember()->getTextStatus($accounting->getStatus2()));
                $worksheet->setCellValue('T' . $i, $accounting->getMember()->getTextStatus($accounting->getStatus3()));
                $worksheet->setCellValue('U' . $i, $accounting->getMember()->getTextStatus($accounting->getStatus4()));

                //  Стилизуем
                $worksheet->duplicateStyle($style, "A{$i}:U{$i}");
            }
        }

        //  Возвращаемся на 1 вкладку
        $worksheet = $spreadsheet->setActiveSheetIndex(0);

        //  Сохранить файл
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
