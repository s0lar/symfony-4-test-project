<?php

namespace App\Reports;

use App\Entity\DepartmentMo;
use App\Entity\Member;
use App\Reports\ReportInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 1
 * Отчет №1
 * Ведомость уплаты членских взносов за квартал
 */
class Report_1 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {
        $po = $data['departmentPo'];
        $data['period'] = $data['quarter'] . ' квартал ' . $data['year'];
        $data['ro'] = 'Краснодарское';
        $data['mo'] = $this->em->getRepository(DepartmentMo::class)->findOneById($po->getMoId())->getTitle();
        $data['po'] = $po->getTitle() . ', ' . $po->getTitleMo();
        $data['members'] = $this->em->getRepository(Member::class)->findForReport1($po->getId(), $data['quarter']);

        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {
        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        //
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_1.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();

        //  Шапка отчета
        $worksheet->setCellValue('A4', $data['period']);
        $worksheet->setCellValue('D6', $data['po']);
        $worksheet->setCellValue('D7', $data['mo']);
        $worksheet->setCellValue('D8', $data['ro']);

        //  Копируем стиль ячейки
        $style = $worksheet->getStyleByColumnAndRow(1, 10);

        //  Ввыводим членов партии
        $row = 9;

        foreach ($data['members'] as $key => $item) {

            /**
             * Указываем в ведомости, что ЧП внес оплату за текущий квартал ранее!
             * Если дебет/кредит больше или равен нулю и оплатили хотя бы 1 рубль,
             * то считаем что предоплату внесли за данный квартал
             */

            //  Внесли больше 0 рублей в любом квартале
            $isPayment = false;

            //  Считаем дебет/креди по кварталам
            for ($i = 1; $i <= $data['quarter']; $i++) {
                if ($item["paymentQuarter{$i}"] > 0) {
                    $isPayment = true;
                }
                $tmp[$i] = $item["paymentQuarter{$i}"] - $item["paymentDeclared{$i}"] + (isset($tmp[$i - 1]) ? $tmp[$i - 1] : 0);
            }

            $row++;
            $worksheet->setCellValue('A' . $row, $key + 1);
            $worksheet->setCellValue('B' . $row, $item['name']);
            $worksheet->setCellValue('C' . $row, $item['ticket'] . ' ');
            $worksheet->setCellValue('D' . $row, $data['quarter']);

            if ($tmp[$data['quarter']] > 0) {
                $worksheet->setCellValue('E' . $row, "Оплачено в " . ($data['quarter'] - 1) . " квартале");
            }

            //  Стилизуем
            $worksheet->duplicateStyle($style, "A{$row}:G{$row}");
        }

        //  Итого
        $row++;
        $worksheet->setCellValue('B' . $row, 'Итого:');
        $worksheet->duplicateStyle($style, "A{$row}:G{$row}");

        //  Футтер
        $row++;
        $worksheet->mergeCells("A" . $row . ":B" . ($row + 2));
        $worksheet->mergeCells("C" . $row . ":G" . ($row + 2));
        $worksheet->setCellValue('A' . $row, 'Сумма прописью');

        //  Стилизуем
        $worksheet->duplicateStyle($style, "A" . $row . ":G" . ($row + 2));

        $row += 3;
        $worksheet->mergeCells("A" . $row . ":B" . ($row + 2));
        $worksheet->setCellValue('A' . $row, 'Лицо, уполномоченное принимать взносы');

        $worksheet->mergeCells("C" . $row . ":G" . ($row + 1));
        $worksheet->mergeCells("C" . ($row + 2) . ":G" . ($row + 2));
        $worksheet->setCellValue('C' . ($row + 2), '                               (подпись.)                        (Ф.И.О.)');

        //  Стилизуем
        $worksheet->duplicateStyle($style, "A" . $row . ":G" . ($row + 2));

        $worksheet->getStyle('A' . $row)->getAlignment()->setWrapText(true);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
