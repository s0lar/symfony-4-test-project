<?php

namespace App\Reports;

use App\Entity\Accounting;
use App\Entity\DepartmentPo;
use App\Entity\Member;
use App\Reports\ReportInterface;
use App\Reports\Utility;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 9
 * Отчет №9
 * Ведомость уплаты членских взносов по Первичным Отделениям за квартал
 */
class Report_9 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {
        //  Период
        $data['period'] = 'По первичным отделениям за ' . $data['quarter'] . ' квартал ' . $data['year'];

        //  Количество Партийцев по Первичным отделениям за период
        $data['po'] = $this->em->getRepository(Member::class)->membersCountByPo(
            $data['mo']->getId(),
            $data['year'],
            $data['quarter']
        );

        //  Количество Партийцев по Первичным отделениям за период
        $data['po_title'] = $this->em->getRepository(DepartmentPo::class)->poTitleByMo(
            $data['mo']->getId()
        );

        //  Выбираем все оплаты по МО за период
        $accounting_raw = $this->em->getRepository(Accounting::class)->findBy([
            'mo' . $data['quarter'] => $data['mo']->getId(),
            'year' => $data['year'],
        ]);

        //  Результат оплаты
        $accounting = [];

        //  Выбираем ПО за квартал
        $getPo = 'getPo' . $data['quarter'];

        foreach ($accounting_raw as $item) {
            //  id PO
            $po = $item->$getPo();

            //  Инициализируем массив данных по ПО
            if (!isset($accounting[$po])) {
                $accounting[$po] = array(
                    'payed' => 0,
                    'prepayed' => 0,
                    'unpayed' => 0,
                    'unpayed_count' => 0,
                );
            }

            //  Суммируем оплаты за квартал
            $accounting[$po]['payed'] += Utility::declaredPayedInQuarter($item, $data['quarter']);

            //  Суммируем авансы за квартал
            $accounting[$po]['prepayed'] += Utility::prePayedInQuarter($item, $data['quarter']);

            //  Суммируем неуплаты за период и неплательщиков
            $unPayed = Utility::unPayedSumInQuarter($item, $data['quarter']);
            if ($unPayed < 0) {
                $accounting[$po]['unpayed'] += $unPayed;
                $accounting[$po]['unpayed_count']++;
            }
        }

        //  Сохраняем результаты оплат
        $data['accounting'] = $accounting;

        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {

        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        //
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_9.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();

        //  Шапка отчета
        $worksheet->setCellValue('A2', $data['period']);
        $worksheet->setCellValue('C3', $data['mo']->getTitle());

        //  Копируем стиль ячейки
        $style = $worksheet->getStyleByColumnAndRow(1, 5);

        //  Ввыводим членов партии
        $row = 5;

        foreach ($data['po'] as $id => $count) {
            $row++;
            //  Номер ПО
            $worksheet->setCellValue(
                'A' . $row,
                (isset($data['po_title'][$id])) ? $data['po_title'][$id] : '-'
            );

            //  Кол-во Членов партии в ПО
            $worksheet->setCellValue('B' . $row, $count);

            if (isset($data['accounting'][$id])) {
                //  Сумма заявленных взносов
                $worksheet->setCellValue('C' . $row, $data['accounting'][$id]['payed']);

                //  Сумма авансовых взносов
                $worksheet->setCellValue('D' . $row, $data['accounting'][$id]['prepayed']);

                //  Сумма неуплат
                $worksheet->setCellValue('E' . $row, -1 * $data['accounting'][$id]['unpayed']);

                //  Кол-во неплательщиков
                $worksheet->setCellValue('F' . $row, $data['accounting'][$id]['unpayed_count']);
            }

            //  Стилизуем
            $worksheet->duplicateStyle($style, "A{$row}:F{$row}");
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
