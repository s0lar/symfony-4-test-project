<?php

namespace App\Reports;

/**
 * Интерфейс отчетов
 */
interface ReportInterface
{
    /**
     * Скачать отчет
     *
     * @param array $data
     * @param string $file
     * @return void
     */
    public function save(array $data, string $file);
}
