<?php

namespace App\Reports;

use App\Entity\Member;
use App\Reports\ReportInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 12
 * Отчет №12
 * Ведомость выдачи партийных билетов
 */
class Report_12 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {
        //  Период
        $data['mo_title'] = $data['mo']->getTitle() . ' МО';
        $data['date'] = \DateTime::createFromFormat('Y.m.d', $data['date']);

        $data['members'] = $this->em->getRepository(Member::class)
            ->findBy([
                'partyTicketDate' => $data['date'],
                'mo' => $data['mo'],
            ]);

        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {
        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        //  Загружаем шаблон
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_12.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();

        //  Шапка отчета
        $worksheet->setCellValue('A3', $data['mo_title']);
        $worksheet->setCellValue('F4', $data['date']->format('d.m.Y'));

        //  Копируем стиль ячейки
        $style = $worksheet->getStyleByColumnAndRow(2, 7);

        //  Порядковый номер
        $key = 0;

        //  Строка
        $row = 6;

        foreach ($data['members'] as $id => $m) {
            $key++;
            $row++;

            //  Порядковый номер
            $worksheet->setCellValue('A' . $row, $key);

            //  Фамилия
            $worksheet->setCellValue('B' . $row, $m->getLastName());

            //  Имя
            $worksheet->setCellValue('C' . $row, $m->getFirstName());

            //  Отчество
            $worksheet->setCellValue('D' . $row, $m->getMiddleName());

            //  № партийного билета
            $worksheet->setCellValue('E' . $row, $m->getPartyTicket());

            //  Дата принятия
            $worksheet->setCellValue('F' . $row, $data['date']->format('d.m.Y'));

            //  Стилизуем
            $worksheet->duplicateStyle($style, "A{$row}:G{$row}");
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
