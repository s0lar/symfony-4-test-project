<?php

namespace App\Reports;

use App\Entity\Accounting;

/**
 * Утилиты для отчетов
 */
class Utility
{
    /**
     * Заявленные поступления за квартал, без аванса
     *
     * @param Accounting $accounting
     * @param integer $quarter
     * @return integer
     */
    public static function declaredPayedInQuarter(Accounting $accounting, $quarter = 1): int
    {
        $declared = "getPaymentDeclared" . $quarter;
        $payment = "getPaymentQuarter" . $quarter;

        //  Если поступило средств столько же или больше чем заявлено, то возвращаем сумму поступлений
        return (int) (($accounting->$payment() - $accounting->$declared() > 0) ? $accounting->$declared() : $accounting->$payment());
    }

    /**
     * Авансовые платежи за квартал
     *
     * @param Accounting $accounting
     * @param integer $quarter
     * @return integer
     */
    public static function prePayedInQuarter(Accounting $accounting, $quarter = 1): int
    {
        $declared = "getPaymentDeclared" . $quarter;
        $payment = "getPaymentQuarter" . $quarter;

        //  Возвращаем авансовый платеж
        return (int) (($accounting->$payment() - $accounting->$declared() > 0) ? $accounting->$payment() - $accounting->$declared() : 0);
    }

    /**
     * Сумма неуплат за квартал
     *
     * @param Accounting $accounting
     * @param integer $quarter
     * @return integer
     */
    public static function unPayedSumInQuarter(Accounting $accounting, $quarter = 1): int
    {
        //  Считаем дебет/кредит по году
        $tmp['total1'] = $accounting->getPaymentQuarter1() - $accounting->getPaymentDeclared1();
        $tmp['total2'] = $accounting->getPaymentQuarter2() - $accounting->getPaymentDeclared2() + $tmp['total1'];
        $tmp['total3'] = $accounting->getPaymentQuarter3() - $accounting->getPaymentDeclared3() + $tmp['total2'];
        $tmp['total4'] = $accounting->getPaymentQuarter4() - $accounting->getPaymentDeclared4() + $tmp['total3'];

        //  Возвращаем сумму неуплат за квартал
        return ($tmp['total' . $quarter] < 0) ? $tmp['total' . $quarter] : 0;
    }

    /**
     * Является ли ЧП неплательщиком в заданом квартале?
     *
     * ЧП мог внести авансом сумму за весь год в 1 квартале,
     * поэтому нельзя его записывать в должники в 2,3,4 кварталах
     *
     * @param Accounting $accounting
     * @param integer $quarter
     * @return integer|null
     */
    public static function isUnPayedInQuarter(Accounting $accounting, $quarter = 1)
    {
        //  По-умолчанию никто не должник
        $result = array(
            1 => false,
            2 => false,
            3 => false,
            4 => false,
        );

        //  Считаем дебет/кредит по году
        $debet[1] = $accounting->getPaymentQuarter1() - $accounting->getPaymentDeclared1();
        $debet[2] = $accounting->getPaymentQuarter2() - $accounting->getPaymentDeclared2() + $debet[1];
        $debet[3] = $accounting->getPaymentQuarter3() - $accounting->getPaymentDeclared3() + $debet[2];
        $debet[4] = $accounting->getPaymentQuarter4() - $accounting->getPaymentDeclared4() + $debet[3];

        //  Определяем должника в 1 квартале
        if ($debet[1] < 0 || ($accounting->getPaymentDeclared1() === 0 && $accounting->getPaymentQuarter1() === null)) {
            $result[1] = true;
        }
        if ($debet[1] > 0) {
            $result[1] = false;
        }

        //  Определяем должника в 2 квартале
        if ($debet[2] < 0 || ($accounting->getPaymentDeclared2() === 0 && $accounting->getPaymentQuarter2() === null)) {
            $result[2] = true;
        }
        if ($debet[2] > 0) {
            $result[2] = false;
        }

        //  Определяем должника в 3 квартале
        if ($debet[3] < 0 || ($accounting->getPaymentDeclared3() === 0 && $accounting->getPaymentQuarter3() === null)) {
            $result[3] = true;
        }
        if ($debet[3] > 0) {
            $result[3] = false;
        }

        //  Определяем должника в 4 квартале
        if ($debet[4] < 0 || ($accounting->getPaymentDeclared4() === 0 && $accounting->getPaymentQuarter4() === null)) {
            $result[4] = true;
        }
        if ($debet[4] > 0) {
            $result[4] = false;
        }

        return $result[$quarter];
    }

    /**
     * Является ли ЧП плательщиком в заданом квартале?
     *
     * @param Accounting $accounting
     * @param integer $quarter
     * @return integer|null
     */
    public static function isPayedInQuarter(Accounting $accounting, $quarter = 1)
    {
        //  По-умолчанию никто не платил
        $result = array(
            1 => false,
            2 => false,
            3 => false,
            4 => false,
        );

        //  Считаем дебет/кредит по году
        $debet[1] = $accounting->getPaymentQuarter1() - $accounting->getPaymentDeclared1();
        $debet[2] = $accounting->getPaymentQuarter2() - $accounting->getPaymentDeclared2() + $debet[1];
        $debet[3] = $accounting->getPaymentQuarter3() - $accounting->getPaymentDeclared3() + $debet[2];
        $debet[4] = $accounting->getPaymentQuarter4() - $accounting->getPaymentDeclared4() + $debet[3];

        //  Определяем должника в 1 квартале
        if ($debet[1] > 0) {
            $result[1] = true;
        } elseif ($debet[1] == 0 && $accounting->getPaymentQuarter1() !== null && $accounting->getPaymentQuarter1() >= 0 && $accounting->getPaymentDeclared1() >= 0) {
            $result[1] = true;
        }

        //  Определяем должника в 2 квартале
        if ($debet[2] > 0) {
            $result[2] = true;
        } elseif ($debet[2] == 0 && $accounting->getPaymentQuarter2() !== null && $accounting->getPaymentQuarter2() >= 0 && $accounting->getPaymentDeclared2() >= 0) {
            $result[2] = true;
        }

        //  Определяем должника в 3 квартале
        if ($debet[3] > 0) {
            $result[3] = true;
        } elseif ($debet[3] == 0 && $accounting->getPaymentQuarter3() !== null && $accounting->getPaymentQuarter3() >= 0 && $accounting->getPaymentDeclared3() >= 0) {
            $result[3] = true;
        }

        //  Определяем должника в 4 квартале
        if ($debet[4] > 0) {
            $result[4] = true;
        } elseif ($debet[4] == 0 && $accounting->getPaymentQuarter4() !== null && $accounting->getPaymentQuarter4() >= 0 && $accounting->getPaymentDeclared4() >= 0) {
            $result[4] = true;
        }

        return $result[$quarter];
    }

    /**
     * Принадлежит ли запись о взносе заданному МО и кварталу
     *
     * @param Accounting $accounting
     * @param integer $mo
     * @param integer $quarter
     * @return boolean
     */
    public static function isInMoThisQuarter(Accounting $accounting, int $mo, int $quarter): bool
    {
        $method = 'getMo' . $quarter;
        return ($accounting->$method() == $mo);
    }

    /**
     * Принадлежит ли запись о взносе заданному МО и кварталу
     *
     * @param Accounting $accounting
     * @param integer $mo
     * @param integer $quarter
     * @return boolean
     */
    public static function isInMoThisQuarterOrNull(Accounting $accounting, int $mo, int $quarter): bool
    {
        $method = 'getMo' . $quarter;

        //  Если МО в квартале еще не задано, то сравниваем с МО текущим
        if ($accounting->$method() === null) {
            return ($accounting->getMo() == $mo);
        } else {
            return ($accounting->$method() == $mo);
        }
    }

    /**
     * Дата по русски
     *
     * @param DateTime $date
     * @return mixed
     */
    public static function prepareDay(\DateTime $date)
    {
        $month = array(
            "01" => "января",
            "02" => "февраля",
            "03" => "марта",
            "04" => "апреля",
            "05" => "мая",
            "06" => "июня",
            "07" => "июля",
            "08" => "августа",
            "09" => "сентября",
            "10" => "октября",
            "11" => "ноября",
            "12" => "декабря",
        );

        $t = $date->getTimestamp();

        $d = strftime("%e", $t);
        $m = strftime("%m", $t);
        $y = strftime("%Y", $t);

        return sprintf("%s %s %s", $d, $month[$m], $y);
    }
}
