<?php

namespace App\Reports;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\Member;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 5
 * Отчет №5
 * Количественный и качественный состав членов Партии
 */
class Report_5 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {
        //  Период
        $data['period'] = "по состоянию на "
        . Utility::prepareDay($data['date'])
            . " г.";

        //  Кол-во по полу
        $data['total'] = $this->em->getRepository(Member::class)->membersCountTotal();
        $data['total_ticket'] = $this->em->getRepository(Member::class)->membersCountByPartyTicket();

        //  Кол-во по полу
        $data['sex'] = $this->em->getRepository(Member::class)->membersCountBySex($data['date']);

        //  Кол-во по образованию
        $data['education'] = $this->em->getRepository(Member::class)->membersCountByEducation($data['date']);

        //  Кол-во по возрасту
        $data['age']['25'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 0, 25);
        $data['age']['30'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 25, 30);
        $data['age']['35'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 30, 35);
        $data['age']['40'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 35, 40);
        $data['age']['45'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 40, 45);
        $data['age']['50'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 45, 50);
        $data['age']['55'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 50, 55);
        $data['age']['60'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 55, 60);
        $data['age']['65'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 60, 65);
        $data['age']['70'] = $this->em->getRepository(Member::class)->membersCountByAge($data['date'], 65, 200);

        //  Кол-во по образованию
        $data['social'] = $this->em->getRepository(Member::class)->membersCountBySocial($data['date']);
        $data['work'] = $this->em->getRepository(Member::class)->membersCountByWork($data['date']);

        $data['mo_count'] = $this->em->getRepository(DepartmentMo::class)->countMo();
        $data['po_count'] = $this->em->getRepository(DepartmentPo::class)->countPo();

        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {
        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        //
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_5.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();

        //  Шапка отчета
        $worksheet->setCellValue('A4', $data['period']);

        //  Итого
        $worksheet->setCellValue(
            'D7',
            isset($data['total']['count']) ? $data['total']['count'] : 0
        );

        //  Мужчины
        $worksheet->setCellValue(
            'D9',
            isset($data['sex'][0]) ? $data['sex'][0] : 0
        );

        //  Женщины
        $worksheet->setCellValue(
            'D10',
            isset($data['sex'][1]) ? $data['sex'][1] : 0
        );

        //  Образование
        $worksheet->setCellValue('D11', isset($data['education'][1]) ? $data['education'][1] : 0);
        $worksheet->setCellValue('D12', isset($data['education'][2]) ? $data['education'][2] : 0);
        $worksheet->setCellValue('D13', isset($data['education'][3]) ? $data['education'][3] : 0);
        $worksheet->setCellValue('D14', isset($data['education'][4]) ? $data['education'][4] : 0);
        $worksheet->setCellValue('D15', isset($data['education'][5]) ? $data['education'][5] : 0);

        //  Возраст
        $worksheet->setCellValue('D16', isset($data['age'][25]['count']) ? $data['age'][25]['count'] : 0);
        $worksheet->setCellValue('D17', isset($data['age'][30]['count']) ? $data['age'][30]['count'] : 0);
        $worksheet->setCellValue('D18', isset($data['age'][35]['count']) ? $data['age'][35]['count'] : 0);
        $worksheet->setCellValue('D19', isset($data['age'][40]['count']) ? $data['age'][40]['count'] : 0);
        $worksheet->setCellValue('D20', isset($data['age'][45]['count']) ? $data['age'][45]['count'] : 0);
        $worksheet->setCellValue('D21', isset($data['age'][50]['count']) ? $data['age'][50]['count'] : 0);
        $worksheet->setCellValue('D22', isset($data['age'][55]['count']) ? $data['age'][55]['count'] : 0);
        $worksheet->setCellValue('D23', isset($data['age'][60]['count']) ? $data['age'][60]['count'] : 0);
        $worksheet->setCellValue('D24', isset($data['age'][65]['count']) ? $data['age'][65]['count'] : 0);
        $worksheet->setCellValue('D25', isset($data['age'][70]['count']) ? $data['age'][70]['count'] : 0);

        //  Социальная категория
        $worksheet->setCellValue('D26', isset($data['social'][1]) ? $data['social'][1] : 0);
        $worksheet->setCellValue('D27', isset($data['social'][2]) ? $data['social'][2] : 0);
        $worksheet->setCellValue('D28', isset($data['social'][3]) ? $data['social'][3] : 0);
        $worksheet->setCellValue('D29', isset($data['social'][4]) ? $data['social'][4] : 0);
        $worksheet->setCellValue('D30', isset($data['social'][5]) ? $data['social'][5] : 0);
        $worksheet->setCellValue('D31', isset($data['social'][6]) ? $data['social'][6] : 0);
        $worksheet->setCellValue('D32', isset($data['social'][7]) ? $data['social'][7] : 0);

        //  Работа
        $worksheet->setCellValue('D33', isset($data['work'][1]) ? $data['work'][1] : 0);
        $worksheet->setCellValue('D34', isset($data['work'][2]) ? $data['work'][2] : 0);
        $worksheet->setCellValue('D35', isset($data['work'][3]) ? $data['work'][3] : 0);
        $worksheet->setCellValue('D36', isset($data['work'][4]) ? $data['work'][4] : 0);
        $worksheet->setCellValue('D37', isset($data['work'][5]) ? $data['work'][5] : 0);
        $worksheet->setCellValue('D38', isset($data['work'][6]) ? $data['work'][6] : 0);
        $worksheet->setCellValue('D39', isset($data['work'][7]) ? $data['work'][7] : 0);
        $worksheet->setCellValue('D40', isset($data['work'][8]) ? $data['work'][8] : 0);
        $worksheet->setCellValue('D41', isset($data['work'][9]) ? $data['work'][9] : 0);

        $worksheet->setCellValue('D42', isset($data['mo_count']['count']) ? $data['mo_count']['count'] : 0);
        $worksheet->setCellValue('D43', isset($data['po_count']['count']) ? $data['po_count']['count'] : 0);
        $worksheet->setCellValue('D45', isset($data['total']['count']) ? $data['total']['count'] : 0);
        $worksheet->setCellValue('D46', isset($data['total_ticket']['count']) ? $data['total_ticket']['count'] : 0);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
