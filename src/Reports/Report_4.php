<?php

namespace App\Reports;

use App\Entity\DepartmentMo;
use App\Entity\Member;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 4
 * Отчет №4
 * Реестр местных отделений Партии "ЕДИНАЯ РОССИЯ"
 */
class Report_4 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {
        //  Период
        $data['year'] = (int) $data['date']->format("Y");
        $data['month'] = (int) $data['date']->format("m");

        //  Шапка отчета
        $data['period'] = "Прием в партию с начала " . $data['year'] . " г.";
        $data['day'] = 'Кол-во членов Партии на ' . $data['date']->format("d.m.Y");

        //  Все местные отделения
        $data['mo'] = $this->em->getRepository(DepartmentMo::class)->findAll();

        //  Количество всех Членов партии за выбранную дату
        $data['members_total'] = $this->em->getRepository(Member::class)->membersCountByMo($data['date']);

        //  Выбираем членов партии по месяцам
        for ($i = 1; $i <= 12; $i++) {
            // membersIntervalCountByMo
            $date_from = new \DateTime($data['year'] . '-' . (($i < 10) ? '0' . $i : $i) . '-01');
            $date_to = clone $date_from;

            $data['month_' . $i] = $this->em->getRepository(Member::class)->membersIntervalCountByMo(
                $date_from,
                $date_to->modify('+1 month')
            );
        }

        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {
        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        //
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_4.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();

        //  Шапка отчета
        $worksheet->setCellValue('A1', $data['period']);
        $worksheet->setCellValue('B2', $data['day']);

        //  Копируем стиль ячейки
        $style = $worksheet->getStyleByColumnAndRow(1, 4);

        //  Ввыводим членов партии
        $row = 3;

        foreach ($data['mo'] as $mo) {
            $row++;
            $id = $mo->getId();

            $worksheet->setCellValue('A' . $row, $mo->getTitle());

            //  Кол-во Членов на текущую дату
            if (isset($data['members_total'][$id])) {
                $worksheet->setCellValue('B' . $row, $data['members_total'][$id]);
            }

            //  Добавлено членов партии по месяцам
            if (isset($data['month_1'][$id])) {
                $worksheet->setCellValue('C' . $row, $data['month_1'][$id]);
            }
            if (isset($data['month_2'][$id])) {
                $worksheet->setCellValue('D' . $row, $data['month_2'][$id]);
            }
            if (isset($data['month_3'][$id])) {
                $worksheet->setCellValue('E' . $row, $data['month_3'][$id]);
            }
            if (isset($data['month_4'][$id])) {
                $worksheet->setCellValue('F' . $row, $data['month_4'][$id]);
            }
            if (isset($data['month_5'][$id])) {
                $worksheet->setCellValue('G' . $row, $data['month_5'][$id]);
            }
            if (isset($data['month_6'][$id])) {
                $worksheet->setCellValue('H' . $row, $data['month_6'][$id]);
            }
            if (isset($data['month_7'][$id])) {
                $worksheet->setCellValue('I' . $row, $data['month_7'][$id]);
            }
            if (isset($data['month_8'][$id])) {
                $worksheet->setCellValue('J' . $row, $data['month_8'][$id]);
            }
            if (isset($data['month_9'][$id])) {
                $worksheet->setCellValue('K' . $row, $data['month_9'][$id]);
            }
            if (isset($data['month_10'][$id])) {
                $worksheet->setCellValue('L' . $row, $data['month_10'][$id]);
            }
            if (isset($data['month_11'][$id])) {
                $worksheet->setCellValue('M' . $row, $data['month_11'][$id]);
            }
            if (isset($data['month2_1'][$id])) {
                $worksheet->setCellValue('N' . $row, $data['month_12'][$id]);
            }

            //  Стилизуем
            $worksheet->duplicateStyle($style, "A{$row}:N{$row}");
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
