<?php

namespace App\Reports;

use App\Entity\Accounting;
use App\Entity\Member;
use App\Reports\ReportInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 10
 * Отчет №10
 * Сверка уполаты по всем членам партии за период по Местному отделению
 */
class Report_10 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {
        //  Период
        $data['period'] = 'за ' . $data['quarter'] . ' квартал ' . $data['year'];

        $data['members'] = $this->em->getRepository(Member::class)
            ->allMembersJoinAccounting($data['mo']->getId(), $data['year']);

        $data['accounting'] = $this->em->getRepository(Accounting::class)
            ->accountingByMoIndexByMember($data['mo']->getId(), $data['year'], $data['quarter']);

        // dump($data['accounting']);
        // exit;
        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {
        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        $member = new Member();
        $status = $member->getStatusList();

        //
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_10.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();

        //  Шапка отчета
        $worksheet->setCellValue('A4', $data['period']);
        $worksheet->setCellValue('D6', $data['mo']->getTitle());

        //  Копируем стиль ячейки
        $style = $worksheet->getStyleByColumnAndRow(1, 9);

        //  Ввыводим членов партии
        $row = 8;

        $quarter = 'paymentQuarter' . $data['quarter'];

        //  Порядковый номер
        $key = 0;

        foreach ($data['members'] as $id => $m) {
            $key++;
            $row++;

            //  Порядковый номер
            $worksheet->setCellValue('A' . $row, $key);

            //  Имя
            $worksheet->setCellValue('B' . $row, $m['name']);

            //  Имя
            $worksheet->setCellValue('C' . $row, $m['ticket']);
            $worksheet->setCellValue(
                'D' . $row,
                isset($status[$m['status']]) ? $status[$m['status']] : "-"
            );
            $worksheet->setCellValue(
                'E' . $row,
                isset($m['acceptedDate']) ? $m['acceptedDate']->format('d.m.Y') : ''
            );
            $worksheet->setCellValue(
                'F' . $row,
                isset($m['partyExitDate']) ? $m['partyExitDate']->format('d.m.Y') : ''
            );

            $worksheet->setCellValue(
                'G' . $row,
                isset($data['accounting'][$id][$quarter]) ? $data['accounting'][$id][$quarter] : ''
            );

            //  Стилизуем
            $worksheet->duplicateStyle($style, "A{$row}:G{$row}");
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
