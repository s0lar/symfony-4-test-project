<?php

namespace App\Reports;

use App\Entity\Member;
use App\Reports\ReportInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Report 11
 * Отчет №11
 * Выгрузка членов партии по фильтрам
 */
class Report_11 implements ReportInterface
{
    private $em;

    /**
     * Конструктор
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Обрабатываем данные для генерации отчета
     *
     * @param array $data
     * @return array $data
     */
    protected function prepareData(array $data)
    {

        //  Количество Партийцев по Первичным отделениям за период
        $data['members'] = $this->em->getRepository(Member::class)->findMembers(1, $data['filter'], false);

        //  Возвращаем подготовленные данные
        return $data;
    }

    /**
     * Генерируем и сохраняем отчет
     *
     * @param array $data
     * @param string $file
     * @return string
     */
    public function save(array $data, string $file)
    {
        //  Загружаем и обрабатываем данные для генерации отчета
        $data = $this->prepareData($data);

        $fields = array(
            'mo' => 'МО',
            'po' => 'ПО',
            'status' => 'Статус',
            'lastName' => 'Фамилия',
            'firstName' => 'Имя',
            'middleName' => 'Отчество',
            'birthday' => 'Дата рождения',
            'sex' => 'Пол',
            'city' => 'Город',
            'street' => 'Улица',
            'post' => 'Индекс',
            'house' => 'Дом',
            'block' => 'Кор',
            'flat' => 'Кв.',
            'passportSeries' => 'Серия',
            'passportNumber' => 'Номер',
            'passportDepartment' => 'Кем выдан',
            'passportDate' => 'Дата выдачи',
            'adherentDate' => 'Сторонник с',
            'requestDate' => 'Дата заявления',
            'phoneHome' => 'Тел. дом.',
            'phoneWork' => 'Тел. раб.',
            'phoneMobile' => 'Тел. моб.',
            'email' => 'email',
            'education' => 'Образование',
            'degree' => 'Степень',
            'rank' => 'Звание',
            'workPlace' => 'Место работы',
            'workPost' => 'Должность',
            'socialCategory' => 'Социальная категория',
            'workSphere' => 'Сфера деятельности',
            'govermentMember' => 'Участие в органах',
            'govermentDateStart' => 'Год начала',
            'govermentDateEnd' => 'Год окончания',
            'govermentPlace' => 'Место работы',
            'govermentLevel' => 'Уровень',
            'govermentPost' => 'Должность',
            'govermentDeputy' => 'Депутат?',
            'govermentDeputyLevel' => 'Депутат, уровень',
            'govermentDeputyPlace' => 'Депутат, наименование органа',
            'partyIn' => 'Выборные и руководящие органы партии',
            'partyPlace' => 'Должность в партии',
            'protocol' => '№ протокола',
            'departmentName' => 'Название отделения',
            'acceptedDate' => 'Дата принятия',
            'pollingDepartment' => '№ избирательного участка',
            'partyTicket' => '№ партийного билета',
            'partyTicketDate' => 'Дата получения партийного билета',
            'partyExitDate' => 'Дата выбытия',
        );

        //
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__ . '/xls/report_11.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();

        $row = 1;
        $col = 1;

        //  Копируем стиль ячейки
        $style = $worksheet->getStyleByColumnAndRow(2, 1);

        //  Шапка выгрузки
        foreach ($fields as $name) {
            $worksheet->setCellValueByColumnAndRow($col, $row, $name);
            $col++;
        }

        $row = 2;

        //  Члены партии
        foreach ($data['members'] as $member) {
            $col = 1;
            foreach ($fields as $key => $name) {
                $method = 'get' . ucfirst($key);

                //  Пол
                if ($key == 'sex') {
                    $worksheet->setCellValueByColumnAndRow(
                        $col,
                        $row,
                        $member->$method() == true ? 'жен' : 'муж'
                    );
                }

                //  Да/нет
                elseif (in_array($key, ['partyIn', 'govermentDeputy', 'govermentMember'])) {
                    $worksheet->setCellValueByColumnAndRow(
                        $col,
                        $row,
                        $member->$method() == 1 ? 'Да' : ''
                    );
                }

                //  Справочники
                elseif (in_array($key, ['status', 'education', 'degree', 'rank', 'socialCategory', 'workSphere', 'govermentPlace', 'govermentLevel', 'govermentDeputyLevel', 'partyPlace'])) {
                    $worksheet->setCellValueByColumnAndRow(
                        $col,
                        $row,
                        $member->$method(true)
                    );
                }

                //  Даты
                elseif (in_array($key, ['birthday', 'requestDate', 'passportDate', 'adherentDate', 'acceptedDate', 'partyTicketDate', 'partyExitDate'])) {
                    $worksheet->setCellValueByColumnAndRow(
                        $col,
                        $row,
                        $member->$method() == null ? '' : $member->$method()->format('d.m.Y')
                    );
                }

                //  Остальные поля
                else {
                    $worksheet->setCellValueByColumnAndRow($col, $row, (string) $member->$method());
                }

                $col++;
            }

            $row++;
            //  Стилизуем
            // $worksheet->duplicateStyle($style, "A{$row}:F{$row}");
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return $file;
    }
}
