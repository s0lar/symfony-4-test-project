<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    protected $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $content = $this->getTwig()
            ->render('security/403.html.twig', ['message' => $accessDeniedException->getMessage()]);
        return new Response($content, 403);
    }

    /**
     * Get the value of twig
     */
    public function getTwig()
    {
        return $this->twig;
    }
}
