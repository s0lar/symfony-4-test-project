<?php

namespace App\Form;

use App\Entity\Accounting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //  Оплата по кварталам
            ->add('paymentQuarter1', null, array(
                'attr' => ['readonly' => ($options['quarter'] == 1) ? false : true],
                'label' => false,
            ))
            ->add('paymentQuarter2', null, array(
                'attr' => ['readonly' => ($options['quarter'] == 2) ? false : true],
                'label' => false,
            ))
            ->add('paymentQuarter3', null, array(
                'attr' => ['readonly' => ($options['quarter'] == 3) ? false : true],
                'label' => false,
            ))
            ->add('paymentQuarter4', null, array(
                'attr' => ['readonly' => ($options['quarter'] == 4) ? false : true],
                'label' => false,
            ))

            //  Заявленная сумма к оплате по кварталам
            ->add('paymentDeclared1', null, array(
                'attr' => ['readonly' => ($options['quarter'] == 1) ? false : true],
                'label' => false,
            ))
            ->add('paymentDeclared2', null, array(
                'attr' => ['readonly' => ($options['quarter'] == 2) ? false : true],
                'label' => false,
            ))
            ->add('paymentDeclared3', null, array(
                'attr' => ['readonly' => ($options['quarter'] == 3) ? false : true],
                'label' => false,
            ))
            ->add('paymentDeclared4', null, array(
                'attr' => ['readonly' => ($options['quarter'] == 4) ? false : true],
                'label' => false,
            ))

            //  Даты оплаты по кварталам
            ->add('dateQuarter1', DateType::class, array(
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'date',
                ],
            ))
            ->add('dateQuarter2', DateType::class, array(
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'date',
                ],
            ))
            ->add('dateQuarter3', DateType::class, array(
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'date',
                ],
            ))
            ->add('dateQuarter4', DateType::class, array(
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'date',
                ],
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Accounting::class,
            'quarter' => 1,
        ]);
    }
}
