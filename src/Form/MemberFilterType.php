<?php

namespace App\Form;

use App\Entity\DepartmentMo;
use App\Entity\DepartmentPo;
use App\Entity\Member;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MemberFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $member = new Member();
        $builder
            ->add('status', ChoiceType::class, array(
                'label' => 'Статус',
                'placeholder' => '..статус..',
                'required' => false,
                'choices' => array_flip($member->getStatusList()),
                'attr' => [
                    'placeholder' => 'Статус',
                    'class' => 'form-control',
                ],
            ))
            ->add('last_name', TextType::class, array(
                'label' => 'Фамилия',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Фамилия',
                    'class' => 'form-control',
                ],
            ))
            ->add('first_name', TextType::class, array(
                'label' => 'Имя',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Имя',
                    'class' => 'form-control',
                ],
            ))
            ->add('middle_name', TextType::class, array(
                'label' => 'Отчество',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Отчество',
                    'class' => 'form-control',
                ],
            ))
            ->add('passport_number', TextType::class, array(
                'label' => 'Паспорт номер',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Паспорт номер',
                    'class' => 'form-control',
                ],
            ))
            ->add('birthday', TextType::class, array(
                'label' => 'День рождения',
                'required' => false,
                'attr' => [
                    // 'placeholder' => date('d.m.Y'),
                    'placeholder' => 'День рождения',
                    'class' => 'form-control birthday-today',
                ],
            ))
            ->add('party_ticket', TextType::class, array(
                'label' => '№ билета',
                'required' => false,
                'attr' => [
                    'placeholder' => '№ билета',
                    'class' => 'form-control',
                ],
            ))
            ->add('mo', EntityType::class, array(
                'class' => DepartmentMo::class,
                'choice_label' => 'title',
                'placeholder' => '..МО не выбрано..',
                'required' => false,
                'label' => 'Первичное отделение',
                'choices' => $options['mo'],
                'attr' => [
                    'class' => 'form-control select2',
                ],
            ))
            ->add('po', EntityType::class, array(
                'class' => DepartmentPo::class,
                'choice_label' => 'title',
                'placeholder' => '..ПО не выбрано..',
                'required' => false,
                'label' => 'Первичное отделение',
                'choices' => $options['po'],
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'group_by' => function ($choiceValue, $key, $value) use ($options) {
                    return isset($options['department_mo'][$choiceValue->getMoId()]) ? $options['department_mo'][$choiceValue->getMoId()] : '';
                },
            ))
            ->add('sort_field', HiddenType::class, array(
                'label' => 'sort_field',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Имя',
                    'class' => 'form-control',
                ],
            ))
            ->add('sort_type', HiddenType::class, array(
                'label' => 'sort_type',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Имя',
                    'class' => 'form-control',
                ],
            ))
            ->add('xls_report', SubmitType::class, array(
                'label' => 'XLS',
                'attr' => [
                    'class' => 'btn btn-outline-success',
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // 'data_class' => Member::class,
            'mo' => [],
            'po' => [],
            'department_mo' => null,
        ]);
    }
}
