<?php

namespace App\Form;

use App\Entity\DepartmentPo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class MemberType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        // Листнер. Не отрисовываем часть полей при редактировании Членов партии пользователями с ролью МИК!
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($user, $options) {
            $member = $event->getData();
            $form = $event->getForm();

            //  Показываем все поля для заполнения, если
            //  - новый член партии
            //  - роль пользователя АДМИН
            if (in_array('ROLE_ADMIN', $user->getRoles()) || $member->getId() === null) {
                $form
                    ->add('status', ChoiceType::class, array(
                        'label' => 'Статус',
                        'placeholder' => '..не выбрано..',
                        'required' => false,
                        'choices' => array_flip($options['data']->getStatusList()),
                        'attr' => [
                            'placeholder' => 'Статус',
                            'class' => 'form-control',
                        ],
                    ))
                    ->add('last_name', TextType::class, array(
                        'label' => 'Фамилия',
                        'required' => false,
                        'attr' => [
                            'placeholder' => 'Фамилия',
                            'class' => 'form-control',
                        ],
                    ))
                    ->add('first_name', TextType::class, array(
                        'label' => 'Имя',
                        'required' => false,
                        'attr' => [
                            'placeholder' => 'Имя',
                            'class' => 'form-control',
                        ],
                    ))
                    ->add('middle_name', TextType::class, array(
                        'label' => 'Отчество',
                        'required' => false,
                        'attr' => [
                            'placeholder' => 'Отчество',
                            'class' => 'form-control',
                        ],
                    ))
                    ->add('birthday', DateType::class, array(
                        'label' => 'День рождения',
                        'required' => false,
                        'widget' => 'single_text',
                        'attr' => [
                            'placeholder' => 'День рождения',
                            'class' => 'form-control',
                        ],
                    ))
                    ->add('acceptedDate', DateType::class, array(
                        'label' => 'Дата принятия',
                        'required' => false,
                        'widget' => 'single_text',
                        'attr' => [
                            'placeholder' => 'Дата принятия',
                            'class' => 'form-control',
                        ],
                    ))
                    ->add('partyTicket', TextType::class, array(
                        'label' => '№ партийного билета',
                        'required' => false,
                        'attr' => [
                            'placeholder' => '№ партийного билета',
                            'class' => 'form-control',
                        ],
                    ))
                    ->add('partyTicketDate', DateType::class, array(
                        'label' => 'Дата получения партийного билета',
                        'required' => false,
                        'widget' => 'single_text',
                        'attr' => [
                            'placeholder' => 'Дата получения партийного билета',
                            'class' => 'form-control',
                        ],
                    ))
                    ->add('partyExitDate', DateType::class, array(
                        'label' => 'Дата выбытия',
                        'required' => false,
                        'widget' => 'single_text',
                        'attr' => [
                            'placeholder' => 'Дата выбытия',
                            'class' => 'form-control',
                        ],
                    ))
                ;
            }
        });

        $builder
            ->add('po', EntityType::class, array(
                'class' => DepartmentPo::class,
                'choice_label' => 'title',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'label' => 'Первичное отделение',
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'group_by' => function ($choiceValue, $key, $value) use ($options) {
                    return isset($options['department_mo'][$choiceValue->getMoId()]) ? $options['department_mo'][$choiceValue->getMoId()] : '';
                },
            ))
            ->add('sex', ChoiceType::class, array(
                'label' => 'Пол',
                'required' => false,
                'choices' => array(
                    'Муж' => 0,
                    'Жен' => 1,
                ),
                'attr' => [
                    'placeholder' => 'Пол',
                    'class' => 'form-control',
                ],
            ))
            ->add('city', TextType::class, array(
                'label' => 'Город',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Город',
                    'class' => 'form-control',
                ],
            ))
            ->add('street', TextType::class, array(
                'label' => 'Улица',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Улица',
                    'class' => 'form-control',
                ],
            ))
            ->add('post', TextType::class, array(
                'label' => 'Индекс',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Индекс',
                    'class' => 'form-control',
                ],
            ))
            ->add('house', TextType::class, array(
                'label' => 'Дом',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Дом',
                    'class' => 'form-control',
                ],
            ))
            ->add('block', TextType::class, array(
                'label' => 'Корпус',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Корпус',
                    'class' => 'form-control',
                ],
            ))
            ->add('flat', TextType::class, array(
                'label' => 'Квартира',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Квартира',
                    'class' => 'form-control',
                ],
            ))
            ->add('passportSeries', TextType::class, array(
                'label' => 'Серия',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Серия',
                    'class' => 'form-control',
                ],
            ))
            ->add('passportNumber', TextType::class, array(
                'label' => 'Номер',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Номер',
                    'class' => 'form-control',
                ],
            ))
            ->add('passportDepartment', TextType::class, array(
                'label' => 'Кем выдан',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Кем выдан',
                    'class' => 'form-control',
                ],
            ))
            ->add('passportDate', DateType::class, array(
                'label' => 'Дата выдачи',
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'placeholder' => 'Дата выдачи',
                    'class' => 'form-control',
                ],
            ))
            ->add('requestDate', DateType::class, array(
                'label' => 'Дата заявления',
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'placeholder' => 'Дата заявления',
                    'class' => 'form-control',
                ],
            ))
            ->add('phoneHome', TextType::class, array(
                'label' => 'Телефон - домашний',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Телефон - домашний',
                    'class' => 'form-control',
                ],
            ))
            ->add('phoneWork', TextType::class, array(
                'label' => 'Телефон - рабочий',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Телефон - рабочий',
                    'class' => 'form-control',
                ],
            ))
            ->add('phoneMobile', TextType::class, array(
                'label' => 'Телефон - сотовый',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Телефон - сотовый',
                    'class' => 'form-control',
                ],
            ))
            ->add('email', TextType::class, array(
                'label' => 'Email',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Email',
                    'class' => 'form-control',
                ],
            ))
            ->add('education', ChoiceType::class, array(
                'label' => 'Образование',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getEducationList()),
                'attr' => [
                    'placeholder' => 'Образование',
                    'class' => 'form-control',
                ],
            ))
            ->add('degree', ChoiceType::class, array(
                'label' => 'Ученая степень',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getDegreeList()),
                'attr' => [
                    'placeholder' => 'Ученая степень',
                    'class' => 'form-control',
                ],
            ))
            ->add('rank', ChoiceType::class, array(
                'label' => 'Звание',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getRankList()),
                'attr' => [
                    'placeholder' => 'Звание',
                    'class' => 'form-control',
                ],
            ))
            ->add('workPlace', TextType::class, array(
                'label' => 'Место работы',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Место работы',
                    'class' => 'form-control',
                ],
            ))
            ->add('workPost', TextType::class, array(
                'label' => 'Должность',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Должность',
                    'class' => 'form-control',
                ],
            ))
            ->add('socialCategory', ChoiceType::class, array(
                'label' => 'Социальная категория',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getSocialList()),
                'attr' => [
                    'placeholder' => 'Социальная категория',
                    'class' => 'form-control',
                ],
            ))
            ->add('workSphere', ChoiceType::class, array(
                'label' => 'Сфера деятельности',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getWorkList()),
                'attr' => [
                    'placeholder' => 'Сфера деятельности',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentMember', ChoiceType::class, array(
                'label' => 'Участие в органах',
                'required' => false,
                'choices' => array(
                    'Да' => 1,
                    'Нет' => 0,
                ),
                'attr' => [
                    'placeholder' => 'Участие в органах',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentDateStart', TextType::class, array(
                'label' => 'Участие в органах. Дата начала',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Дата начала',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentDateEnd', TextType::class, array(
                'label' => 'Участие в органах. Дата окончания',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Дата окончания',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentPlace', ChoiceType::class, array(
                'label' => 'Участие в органах. Место работы',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getGovermentPlaceList()),
                'attr' => [
                    'placeholder' => 'Участие в органах. Место работы',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentLevel', ChoiceType::class, array(
                'label' => 'Участие в органах. Уровень',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getGovermentLevelList()),
                'attr' => [
                    'placeholder' => 'Участие в органах. Уровень',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentPost', TextType::class, array(
                'label' => 'Должность',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Должность',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentDeputy', ChoiceType::class, array(
                'label' => 'Депутат?',
                'required' => false,
                'choices' => array(
                    'Да' => 1,
                    'Нет' => 0,
                ),
                'attr' => [
                    'placeholder' => 'Депутат?',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentDeputyLevel', ChoiceType::class, array(
                'label' => 'Участие в органах. Депутат уровень',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getGovermentDeputyLevelList()),
                'attr' => [
                    'placeholder' => 'Участие в органах. Депутат уровень',
                    'class' => 'form-control',
                ],
            ))
            ->add('govermentDeputyPlace', TextType::class, array(
                'label' => 'Наименование органа',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Наименование органа',
                    'class' => 'form-control',
                ],
            ))
            ->add('partyIn', ChoiceType::class, array(
                'label' => 'Выборные и руководящие органы партии',
                'required' => false,
                'choices' => array(
                    'Да' => 1,
                    'Нет' => 0,
                ),
                'attr' => [
                    'placeholder' => 'Выборные и руководящие органы партии',
                    'class' => 'form-control',
                ],
            ))
            ->add('partyPlace', ChoiceType::class, array(
                'label' => 'Должность в партии',
                'placeholder' => '..не выбрано..',
                'required' => false,
                'choices' => array_flip($options['data']->getPartyPlaceList()),
                'attr' => [
                    'placeholder' => 'Должность в партии',
                    'class' => 'form-control',
                ],
            ))
            ->add('protocol', TextType::class, array(
                'label' => '№ протокола',
                'required' => false,
                'attr' => [
                    'placeholder' => '№ протокола',
                    'class' => 'form-control',
                ],
            ))
            ->add('pollingDepartment', TextType::class, array(
                'label' => '№ избирательного участка',
                'required' => false,
                'attr' => [
                    'placeholder' => '№ избирательного участка',
                    'class' => 'form-control',
                ],
            ))
            ->add('adherentDate', DateType::class, array(
                'label' => 'Сторонник с',
                'required' => false,
                'widget' => 'single_text',
                'attr' => [
                    'placeholder' => 'Дата выбытия',
                    'class' => 'form-control',
                ],
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            //'data_class' => Member::class,
            'department_mo' => null,
        ]);
    }
}
