<?php

namespace App\Form;

use App\Entity\DepartmentMo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportFilter4Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mo', EntityType::class, array(
                'class' => DepartmentMo::class,
                'choice_label' => 'title',
                'placeholder' => '..не выбрано..',
                'required' => true,
                'label' => 'Местное отделение',
                'attr' => [
                    'class' => 'form-control select2',
                ],
            ))
            ->add('date', DateType::class, array(
                'label' => 'Дата',
                'required' => false,
                'widget' => 'single_text',
                'data' => new \DateTime(),
                'attr' => [
                    'placeholder' => 'Дата',
                    'class' => 'form-control',
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
