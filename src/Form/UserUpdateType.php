<?php

namespace App\Form;

use App\Entity\DepartmentMo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Имя пользователя',
                'attr' => [
                    'placeholder' => 'Имя пользователя',
                    'class' => 'form-control',
                ],
            ))
            ->add('name', TextType::class, array(
                'label' => 'ФИО',
                'required' => false,
                'attr' => [
                    'placeholder' => 'ФИО',
                    'class' => 'form-control',
                ],
            ))
            ->add('email', EmailType::class, array(
                'label' => 'E-mail',
                'attr' => [
                    'placeholder' => 'E-mail',
                    'class' => 'form-control',
                ],
            ))
            ->add('roles', ChoiceType::class, array(
                'label' => 'Права доступа',
                'choices' => array_flip($options['data']->getRolesList()),
                'expanded' => true,
                'multiple' => true,
                'attr' => [
                    'class' => 'form-checkbox',
                ],
            ))
            ->add('department_mo', EntityType::class, array(
                'class' => DepartmentMo::class,
                'choice_label' => 'title',
                'placeholder' => '..не выбрано..',
                'empty_data' => null,
                'required' => false,
                'label' => 'Муниципальное образование',
                'attr' => [
                    'class' => 'form-control',
                ],
            ))
            ->add('isActive', ChoiceType::class, array(
                'label' => 'Активирован?',
                'choices' => array(
                    'Да' => 1,
                    'Нет' => 0,
                ),
                'attr' => [
                    'class' => 'form-control w-25',
                ],
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            // 'data_class' => UserEdit::class,
        ]);
    }
}
