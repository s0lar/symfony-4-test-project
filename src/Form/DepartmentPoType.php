<?php

namespace App\Form;

use App\Entity\DepartmentPo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepartmentPoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', null, array(
                'label' => 'Номер отделения',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Номер отделения',
                    'class' => 'form-control',
                ],
            ))
            ->add('moId', ChoiceType::class, array(
                'choices' => array_flip($options['moId']),
                'placeholder' => '..не выбрано..',
                'required' => true,
                'label' => 'Местное отделение',
                'attr' => [
                    'class' => 'form-control select12',
                ],
            ))
            ->add('zoId', ChoiceType::class, array(
                'choices' => array_flip($options['zoId']),
                'placeholder' => '..не выбрано..',
                'required' => true,
                'label' => 'Округ',
                'attr' => [
                    'class' => 'form-control select12',
                ],
            ))
            ->add('title', null, array(
                'label' => 'Название',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Название',
                    'class' => 'form-control',
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DepartmentPo::class,
            'moId' => array(),
            'zoId' => array(),
        ]);
    }
}
