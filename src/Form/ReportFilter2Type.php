<?php

namespace App\Form;

use App\Entity\DepartmentMo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportFilter2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mo', EntityType::class, array(
                'class' => DepartmentMo::class,
                'choice_label' => 'title',
                'placeholder' => '..не выбрано..',
                'required' => true,
                'label' => 'Муниципальное отделение',
                'attr' => [
                    'class' => 'form-control select2',
                ],
            ))
            ->add('year', TextType::class, array(
                'label' => 'Год',
                'required' => true,
                'data' => date('Y'),
                'attr' => [
                    'placeholder' => 'Год',
                    'class' => 'form-control',
                ],
            ))
            ->add('quarter', ChoiceType::class, array(
                'label' => 'Квартал',
                'required' => true,
                'choices' => array(
                    '1 кв.' => 1,
                    '2 кв.' => 2,
                    '3 кв.' => 3,
                    '4 кв.' => 4,
                ),
                'attr' => [
                    'placeholder' => 'Квартал',
                    'class' => 'form-control',
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
