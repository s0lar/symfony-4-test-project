<?php

namespace App\Form;

use App\Entity\DepartmentMo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepartmentMoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', null, array(
                'label' => 'Номер отделения',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Номер отделения',
                    'class' => 'form-control',
                ],
            ))
            ->add('title', null, array(
                'label' => 'Название',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Название',
                    'class' => 'form-control',
                ],
            ))
            ->add('titleRegion', null, array(
                'label' => 'Название района',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Название района',
                    'class' => 'form-control',
                ],
            ))
            ->add('roId', HiddenType::class, array(
                'data' => '23',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DepartmentMo::class,
        ]);
    }
}
