<?php

namespace App\Form;

use App\Entity\DepartmentZo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepartmentZoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                'label' => 'Название округа',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Название округа',
                    'class' => 'form-control',
                ],
            ))
            ->add('mo', null, array(
                'label' => 'Местное отделение',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Местное отделение',
                    'class' => 'form-control',
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DepartmentZo::class,
        ]);
    }
}
