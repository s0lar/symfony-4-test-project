<?php

namespace App\Form;

use App\Entity\DepartmentMo;
use App\Entity\Member;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketsFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $member = new Member();
        $builder
            ->add('mo', EntityType::class, array(
                'class' => DepartmentMo::class,
                'choice_label' => 'title',
                'placeholder' => '..МО не выбрано..',
                'required' => false,
                'label' => 'Первичное отделение',
                'choices' => $options['mo'],
                'attr' => [
                    'class' => 'form-control select2',
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // 'data_class' => Member::class,
            'mo' => [],
        ]);
    }
}
