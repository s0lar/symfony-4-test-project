<?php

namespace App\Tests\Reports;

use App\Entity\Accounting;
use App\Reports\Utility;
use PHPUnit\Framework\TestCase;

class UtilityTest extends TestCase
{
    /**
     * Тестируем ЧП которые
     *
     * @return void
     */
    public function testIsUnPayedInQuarter1()
    {
        $utility = new Utility();
        $accounting = new Accounting();

        //  1 квартал
        $accounting->setPaymentDeclared1(1);
        $accounting->setPaymentQuarter1(1);

        //  2 квартал
        $accounting->setPaymentDeclared2(0);
        $accounting->setPaymentQuarter2(0);

        //  3 квартал
        $accounting->setPaymentDeclared3(10);
        $accounting->setPaymentQuarter3(20);

        //  4 квартал
        $accounting->setPaymentDeclared4(10);
        $accounting->setPaymentQuarter4(0);

        //  Тестируем 1 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 1);
        $this->assertFalse($result, '1 quarter');

        //  Тестируем 2 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 2);
        $this->assertFalse($result, '2 quarter');

        //  Тестируем 3 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 3);
        $this->assertFalse($result, '3 quarter');

        //  Тестируем 4 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 4);
        $this->assertFalse($result, '4 quarter');
    }

    /**
     * Тестируем должников
     *
     * @return void
     */
    public function testIsUnPayedInQuarter2()
    {
        $utility = new Utility();
        $accounting = new Accounting();

        //  1 квартал
        $accounting->setPaymentDeclared1(0);
        $accounting->setPaymentQuarter1(null);

        //  2 квартал
        $accounting->setPaymentDeclared2(10);
        $accounting->setPaymentQuarter2(0);

        //  3 квартал
        $accounting->setPaymentDeclared3(30);
        $accounting->setPaymentQuarter3(20);

        //  4 квартал
        $accounting->setPaymentDeclared4(10);
        $accounting->setPaymentQuarter4(10);

        //  Тестируем 1 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 1);
        $this->assertTrue($result, '1 quarter');

        //  Тестируем 2 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 2);
        $this->assertTrue($result, '2 quarter');

        //  Тестируем 3 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 3);
        $this->assertTrue($result, '3 quarter');

        //  Тестируем 4 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 4);
        $this->assertTrue($result, '4 quarter');
    }

    /**
     * Тестируем смешанные данные
     *
     * @return void
     */
    public function testIsUnPayedInQuarter3()
    {
        $utility = new Utility();
        $accounting = new Accounting();

        //  1 квартал
        $accounting->setPaymentDeclared1(10);
        $accounting->setPaymentQuarter1(40);

        //  2 квартал
        $accounting->setPaymentDeclared2(10);
        $accounting->setPaymentQuarter2(null);

        //  3 квартал
        $accounting->setPaymentDeclared3(10);
        $accounting->setPaymentQuarter3(null);

        //  4 квартал
        $accounting->setPaymentDeclared4(10);
        $accounting->setPaymentQuarter4(null);

        //  Тестируем 1 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 1);
        $this->assertFalse($result, '1 quarter');

        //  Тестируем 2 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 2);
        $this->assertFalse($result, '2 quarter');

        //  Тестируем 3 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 3);
        $this->assertFalse($result, '3 quarter');

        //  Тестируем 4 квартал
        $result = $utility::isUnPayedInQuarter($accounting, 4);
        $this->assertFalse($result, '4 quarter');
    }
}
