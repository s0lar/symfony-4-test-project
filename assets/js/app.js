var $ = require("jquery");
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require("bootstrap");

// or you can include specific pieces
// require('bootstrap-sass/javascripts/bootstrap/tooltip');

require("devbridge-autocomplete");
require("select2");

// $(".autocomplete").autocomplete({
//   dataType: "json",
//   deferRequestBy: 100,
//   serviceUrl: "/member/find-po/"
// });

$(".select2").select2();

/**
 * Учет членских взносов.
 * Переход по кварталам.
 */
$('#quarter_link').change(function () {
    document.location = $(this).val();
});


/**
 * Скрытые поля для сортировки
 */
var $filterField = $("#member_filter_sort_field");
var $filterType = $("#member_filter_sort_type");

/**
 * Сортировка членов партии
 * Клик на поле сортировки
 */
$("#memberSort a.sort").click(function () {
    var $this = $(this);
    var defaultType = "asc";

    $filterField.val(
        $this.data("field")
    );

    $filterType.val(
        $this.hasClass("asc") ? "DESC" : "ASC"
    );

    $("form[name='member_filter']").submit();
});

/**
 * Сортировка
 */
if ($filterField.val()) {
    $('#memberSort a.sort[data-field="' + $filterField.val() + '"]').addClass(
        $filterType.val() == "DESC" ? "desc" : "asc"
    );
}

/**
 * Учет членских взносов
 * Навигация стрелакми
 */
$("#accounting_form input[type=text]").keydown(function (e) {

    var $input = $(e.target);
    var $field = $input.data('field');
    var $index = $input.data('index');

    switch (e.keyCode) {
        case 38:
            $target = $("#" + $field + "-" + ($index - 1));
            $target.focus();
            break;
        case 40:
            $target = $("#" + $field + "-" + ($index + 1));
            $target.focus();
            break;

        default: return; // exit this handler for other keys
    }

    e.preventDefault();

});

/**
 * Члены партии. Фильтр
 * Дата рождения - сегодня
 */
// $(".birthday-today").each(function () {
//     var $parent = $(this).parent();

//     $parent.append("<a href='#' class='birthday-today-hint'>Сегодня</a>");
// });